package model;

import contract.AnalisisContract;
import dto.DtoAnalisis;
import utils.JsonDb;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class Analisis implements AnalisisContract {

    private int idAnalisis;

    public String getNombreAnalisis() {
        return nombreAnalisis;
    }

    private String nombreAnalisis;
    private int valorNormal;
    private int valorCritico;
    private int valorReservado;
    private String resultado;

    private static List<Analisis> listaAnalisis;

    public Boolean isAnalisis(String idAnalisis) {
        return idAnalisis == idAnalisis;
    }

    public Analisis() {
    }

    static {
        listaAnalisis = utils.JsonDb.getAll(Analisis.class);
    }

    public Analisis(int idAnalisis, String nombreAnalisis, int valorNormal, int valorCritico, int valorReservado) {
        this.idAnalisis = idAnalisis;
        this.nombreAnalisis = nombreAnalisis;
        this.valorNormal = valorNormal;
        this.valorCritico = valorCritico;
        this.valorReservado = valorReservado;
    }

    //-- GETTER AND SETTER--//

    public void setNombreAnalisis(String nombreAnalisis) {
        this.nombreAnalisis = nombreAnalisis;
    }

    public static List<Analisis> getListaAnalisis() {
        return listaAnalisis;
    }

    public static void setListaAnalisis(List<Analisis> listaAnalisis) {
        Analisis.listaAnalisis = listaAnalisis;
    }

    public int getIdAnalisis() {
        return idAnalisis;
    }

    public void setIdAnalisis(int idAnalisis) {
        this.idAnalisis = idAnalisis;
    }

    public int getValorNormal() {
        return valorNormal;
    }

    public void setValorNormal(int valorNormal) {
        this.valorNormal = valorNormal;
    }

    public int getValorCritico() {
        return valorCritico;
    }

    public void setValorCritico(int valorCritico) {
        this.valorCritico = valorCritico;
    }

    public int getValorReservado() {
        return valorReservado;
    }

    public void setValorReservado(int valorReservado) {
        this.valorReservado = valorReservado;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }


    @Override
    public DtoAnalisis obtenerAnalisis(int idAnalisis) {
        DtoAnalisis response = null;
        for (Analisis listaAnalisis : listaAnalisis) {
            if (Objects.equals(listaAnalisis.getIdAnalisis(), idAnalisis)) {
                response = new DtoAnalisis();
                response.setIdAnalisis(listaAnalisis.getIdAnalisis());
                response.setResultado(listaAnalisis.getResultado());
                response.setValorCritico(listaAnalisis.getValorCritico());
                response.setValorNormal(listaAnalisis.getValorNormal());
                response.setValorReservado(listaAnalisis.getValorReservado());
                response.setNombreAnalisis(listaAnalisis.getNombreAnalisis());
                return response;
            }
        }
        return null;
    }

    @Override
    public List<DtoAnalisis> getTodosAnalisis() {
        DtoAnalisis result = null;
        List<DtoAnalisis> analisisList = new ArrayList<>();

        for (int i = 0; i < listaAnalisis.size(); i++) {
            result = new DtoAnalisis();
            result.setIdAnalisis(listaAnalisis.get(i).getIdAnalisis());
            result.setValorNormal(listaAnalisis.get(i).getValorNormal());
            result.setValorReservado(listaAnalisis.get(i).getValorReservado());
            result.setValorCritico(listaAnalisis.get(i).getValorCritico());
            result.setResultado(listaAnalisis.get(i).getResultado());
            result.setNombreAnalisis(listaAnalisis.get(i).getNombreAnalisis());
            analisisList.add(result);
        }
        return analisisList;
    }

    @Override
    public Boolean crearAnalisis(DtoAnalisis nuevoAnalisis) {
        if (this.existeAnalisis(nuevoAnalisis.getIdAnalisis())) {
            return false;
        } else {
            listaAnalisis.add(new Analisis(nuevoAnalisis.getIdAnalisis(), nuevoAnalisis.getNombreAnalisis(), nuevoAnalisis.getValorNormal(), nuevoAnalisis.getValorCritico(), nuevoAnalisis.getValorReservado()));
            JsonDb.save(Analisis.class, listaAnalisis);
            return true;
        }
    }

    private Boolean existeAnalisis(int idAnalisis) {
        for (Analisis listaAnalisis : listaAnalisis) {
            if (Objects.equals(listaAnalisis.getIdAnalisis(), idAnalisis)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Boolean borrarAnalisis(int idAnalisis) {
        for (int i = 0; i < listaAnalisis.size(); i++) {
            if (listaAnalisis.get(i).getIdAnalisis() == idAnalisis) {
                listaAnalisis.remove(i);
                JsonDb.save(Analisis.class, listaAnalisis);
                return true;
            }
        }
        return false;
    }

    @Override
    public Boolean editarAnalisis(DtoAnalisis modificarAnalisis) {
        for (int i = 0; i < listaAnalisis.size(); i++) {
            if (Objects.equals(listaAnalisis.get(i).getIdAnalisis(), modificarAnalisis.getIdAnalisis())) {
                if (modificarAnalisis.getIdAnalisis() > 0)
                    listaAnalisis.get(i).setIdAnalisis(modificarAnalisis.getIdAnalisis());
                if (modificarAnalisis.getValorCritico() > 0)
                    listaAnalisis.get(i).setValorCritico(modificarAnalisis.getValorCritico());
                if (modificarAnalisis.getResultado() != null)
                    listaAnalisis.get(i).setResultado(modificarAnalisis.getResultado());
                if (modificarAnalisis.getValorNormal() > 0)
                    listaAnalisis.get(i).setValorNormal(modificarAnalisis.getValorNormal());
                if (modificarAnalisis.getValorReservado() > 0)
                    listaAnalisis.get(i).setValorReservado(modificarAnalisis.getValorReservado());
                listaAnalisis.get(i).setNombreAnalisis(modificarAnalisis.getNombreAnalisis());
                JsonDb.save(Analisis.class, listaAnalisis);
                return true;
            }
        }
        return false;
    }
}
