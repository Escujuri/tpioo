package contract;

import dto.DtoPracticas;
import dto.DtoResultadoPeticion;

import java.util.List;

public interface ResuladoPeticionesContract {

    Boolean crearResultadoPeticion(DtoResultadoPeticion resultadoPeticion);

    Boolean eliminarResultadoPeticion(int idResultado);

    Boolean actualizarResultadoPeticion(DtoResultadoPeticion peticion);

    List<DtoResultadoPeticion> obtenerResultadoPeticion(int idResultado);

    List<DtoResultadoPeticion> obtenerTodosResultadoPeticion();
}
