package dto;

import model.EstadosPeticiones;

import java.util.Date;
import java.util.List;

public class DtoPeticion {

    private int idPeticion;

    private int idAnalisis;

    private long idPaciente;

    private Boolean tieneObraSocial;

    private Date fechaCarga;

    private Date fechaCalculadaEntrega;

    private List<DtoPracticas> practicas;

    public List<DtoPracticas> getPracticas() {
        return practicas;
    }

    private String descripcion;

    private String nombreSucursal;

    private int setCodObraSocial;

    private String nombreObraSocial;

    private EstadosPeticiones estado;

    public DtoPeticion() {
    }
    // -- GETTER AND SETTER--//

    public int getIdPeticion() {
        return idPeticion;
    }

    public void setIdPeticion(int idPeticion) {
        this.idPeticion = idPeticion;
    }

    public Boolean getTieneObraSocial() {
        return tieneObraSocial;
    }

    public void setTieneObraSocial(Boolean tieneObraSocial) {
        this.tieneObraSocial = tieneObraSocial;
    }

    public Date getFechaCarga() {
        return fechaCarga;
    }

    public void setFechaCarga(Date fechaCarga) {
        this.fechaCarga = fechaCarga;
    }

    public Date getFechaCalculadaEntrega() {
        return fechaCalculadaEntrega;
    }

    public void setFechaCalculadaEntrega(Date fechaCalculadaEntrega) {
        this.fechaCalculadaEntrega = fechaCalculadaEntrega;
    }

    public int getIdAnalisis() {
        return idAnalisis;
    }

    public void setIdAnalisis(int idAnalisis) {
        this.idAnalisis = idAnalisis;
    }

    public DtoPeticion(long idPaciente) {
        this.idPaciente = this.idPaciente;
    }

    public void setPacienteId(long solicitante) {
        this.idPaciente = solicitante;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombreSucursal() {
        return nombreSucursal;
    }

    public void setNombreSucursal(String nombreSucursal) {
        this.nombreSucursal = nombreSucursal;
    }

    public void setSetCodObraSocial(int setCodObraSocial) {
        this.setCodObraSocial = setCodObraSocial;
    }

    public String getNombreObraSocial() {
        return nombreObraSocial;
    }

    public void setNombreObraSocial(String nombreObraSocial) {
        this.nombreObraSocial = nombreObraSocial;
    }

    public long getIdPaciente() {
        return idPaciente;
    }

    public int getSetCodObraSocial() {
        return setCodObraSocial;
    }

    public EstadosPeticiones getEstado() {
        return estado;
    }

    public void setEstado(EstadosPeticiones estado) {
        this.estado = estado;
    }

    public void setPracticas(List<DtoPracticas> practicas) {
        this.practicas = practicas;
    }
}
