package dto;

import java.util.Date;

public class DtoUsuarios {

    private int dni;

    private String nombreUsuario;

    private String nombre;

    private String email;

    private String contrasenia;

    private Date fechaNacimiento;

    private Boolean habilitado;

    private DtoDireccion direccion;

    // -- GETTER AND SETTER--//
    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public Boolean getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(Boolean habilitado) {
        this.habilitado = habilitado;
    }

    public DtoDireccion getDireccion() {
        return direccion;
    }

    public void setDireccion(DtoDireccion direccion) {
        this.direccion = direccion;
    }
}
