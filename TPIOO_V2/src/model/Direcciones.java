package model;

import contract.DireccionesContract;
import dto.DtoDireccion;
import utils.JsonDb;

import java.util.List;

public class Direcciones implements DireccionesContract {

    private int idDireccionUsuario;

    private int idDireccionPaciente;

    private int idDireccionSucursal;

    private static List<Direcciones> listaDireccionesUsuarios;

    private static List<Direcciones> listaDireccionesPaciente;

    private static List<Direcciones> listaDireccionesSucursal;

    private String pais;

    private String provincia;

    private String ciudad;

    private String calle;

    private int altura;

    private int telefono;

    static {
        listaDireccionesUsuarios = utils.JsonDb.getAll(Direcciones.class);
        listaDireccionesPaciente = utils.JsonDb.getAll(Direcciones.class);
        listaDireccionesSucursal = utils.JsonDb.getAll(Direcciones.class);
    }

    //-- GETTER AND SETTER--//
    public int getIdDireccionUsuario() {
        return idDireccionUsuario;
    }

    public void setIdDireccionUsuario(int idDireccionUsuario) {
        this.idDireccionUsuario = idDireccionUsuario;
    }

    public int getIdDireccionPaciente() {
        return idDireccionPaciente;
    }

    public void setIdDireccionPaciente(int idDireccionPaciente) {
        this.idDireccionPaciente = idDireccionPaciente;
    }

    public int getIdDireccionSucursal() {
        return idDireccionSucursal;
    }

    public void setIdDireccionSucursal(int idDireccionSucursal) {
        this.idDireccionSucursal = idDireccionSucursal;
    }

    public List<Direcciones> getListaDireccionesUsuarios() {
        return listaDireccionesUsuarios;
    }

    public void setListaDireccionesUsuarios(List<Direcciones> listaDireccionesUsuarios) {
        Direcciones.listaDireccionesUsuarios = listaDireccionesUsuarios;
    }

    public List<Direcciones> getListaDireccionesPaciente() {
        return listaDireccionesPaciente;
    }

    public void setListaDireccionesPaciente(List<Direcciones> listaDireccionesPaciente) {
        Direcciones.listaDireccionesPaciente = listaDireccionesPaciente;
    }

    public List<Direcciones> getListaDireccionesSucursal() {
        return listaDireccionesSucursal;
    }

    public void setListaDireccionesSucursal(List<Direcciones> listaDireccionesSucursal) {
        Direcciones.listaDireccionesSucursal = listaDireccionesSucursal;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }


    public Direcciones(int idDireccion, String pais, String provincia, String ciudad, String calle, Integer altura, Integer telefono) {
        this.idDireccionUsuario = idDireccion;
        this.pais = pais;
        this.provincia = provincia;
        this.ciudad = ciudad;
        this.calle = calle;
        this.altura = altura;
        this.telefono = telefono;
    }

    public Direcciones() {
    }

    @Override
    public Boolean crearDireccionUsuario(DtoDireccion crearDireccionDto) {
        listaDireccionesUsuarios.add(new Direcciones(
                crearDireccionDto.getIdDireccionUsuario(),
                crearDireccionDto.getPais(),
                crearDireccionDto.getProvincia(),
                crearDireccionDto.getCiudad(),
                crearDireccionDto.getCalle(),
                crearDireccionDto.getAltura(),
                crearDireccionDto.getTelefono()
        ));
        JsonDb.save(Direcciones.class, listaDireccionesUsuarios);
        return true;
    }

    @Override
    public Boolean modificarDireccionUsuario(DtoDireccion modificarDireccionUsuario) {
        for (int i = 0; i < listaDireccionesUsuarios.size(); i++) {
            if (listaDireccionesUsuarios.get(i).getIdDireccionUsuario() == modificarDireccionUsuario.getIdDireccionUsuario()) {
                if (modificarDireccionUsuario.getPais() != null)
                    listaDireccionesUsuarios.get(i).setPais(modificarDireccionUsuario.getPais());
                if (modificarDireccionUsuario.getProvincia() != null)
                    listaDireccionesUsuarios.get(i).setProvincia(modificarDireccionUsuario.getProvincia());
                if (modificarDireccionUsuario.getCiudad() != null)
                    listaDireccionesUsuarios.get(i).setCiudad(modificarDireccionUsuario.getCiudad());
                if (modificarDireccionUsuario.getCalle() != null)
                    listaDireccionesUsuarios.get(i).setCalle(modificarDireccionUsuario.getCalle());
                if (modificarDireccionUsuario.getAltura() >= 0)
                    listaDireccionesUsuarios.get(i).setAltura(modificarDireccionUsuario.getAltura());
                if (modificarDireccionUsuario.getTelefono() >= 0)
                    listaDireccionesUsuarios.get(i).setTelefono(modificarDireccionUsuario.getTelefono());
                JsonDb.save(Direcciones.class, listaDireccionesUsuarios);
                return true;
            }
        }
        return false;
    }


    @Override
    public Boolean borrarDireccionUsuario(int idDireccionUsuario) {
        for (int i = 0; i < listaDireccionesUsuarios.size(); i++) {
            if (listaDireccionesUsuarios.get(i).getIdDireccionUsuario() == idDireccionUsuario) {
                listaDireccionesUsuarios.remove(i);
                JsonDb.save(Direcciones.class, listaDireccionesUsuarios);
                return true;
            }
        }
        return false;
    }

    @Override
    public DtoDireccion obtenerDireccionUsuario(int idDireccionUsuario) {
        DtoDireccion response = null;
        for (Direcciones listaDireccionesUsuario : listaDireccionesUsuarios) {
            if (listaDireccionesUsuario.getIdDireccionUsuario() == idDireccionUsuario) {
                response = new DtoDireccion();
                response.setIdDireccionUsuario(listaDireccionesUsuario.getIdDireccionUsuario());
                response.setPais(listaDireccionesUsuario.getPais());
                response.setProvincia(listaDireccionesUsuario.getProvincia());
                response.setCiudad(listaDireccionesUsuario.getCiudad());
                response.setCalle(listaDireccionesUsuario.getCalle());
                response.setAltura(listaDireccionesUsuario.getAltura());
                response.setTelefono(listaDireccionesUsuario.getTelefono());
                return response;
            }
        }
        return null;
    }

    @Override
    public Boolean crearDireccionPaciente(DtoDireccion altaDireccionPaciente) {
        listaDireccionesPaciente.add(new Direcciones(
                altaDireccionPaciente.getIdDireccionPaciente(),
                altaDireccionPaciente.getPais(),
                altaDireccionPaciente.getProvincia(),
                altaDireccionPaciente.getCiudad(),
                altaDireccionPaciente.getCalle(),
                altaDireccionPaciente.getAltura(),
                altaDireccionPaciente.getTelefono()
        ));
        JsonDb.save(Direcciones.class, listaDireccionesUsuarios);
        return true;
    }

    @Override
    public Boolean modificarDireccionPaciente(DtoDireccion modificarDireccionPaciente) {
        for (int i = 0; i < listaDireccionesPaciente.size(); i++) {
            if (listaDireccionesPaciente.get(i).getIdDireccionPaciente() == modificarDireccionPaciente.getIdDireccionPaciente()) {
                if (modificarDireccionPaciente.getPais() != null)
                    listaDireccionesPaciente.get(i).setPais(modificarDireccionPaciente.getPais());
                if (modificarDireccionPaciente.getProvincia() != null)
                    listaDireccionesPaciente.get(i).setProvincia(modificarDireccionPaciente.getProvincia());
                if (modificarDireccionPaciente.getCiudad() != null)
                    listaDireccionesPaciente.get(i).setCiudad(modificarDireccionPaciente.getCiudad());
                if (modificarDireccionPaciente.getCalle() != null)
                    listaDireccionesPaciente.get(i).setCalle(modificarDireccionPaciente.getCalle());
                if (modificarDireccionPaciente.getAltura() >= 0)
                    listaDireccionesPaciente.get(i).setAltura(modificarDireccionPaciente.getAltura());
                if (modificarDireccionPaciente.getTelefono() >= 0)
                    listaDireccionesPaciente.get(i).setTelefono(modificarDireccionPaciente.getTelefono());
                JsonDb.save(Direcciones.class, listaDireccionesUsuarios);
                return true;
            }
        }
        return false;
    }

    @Override
    public Boolean borrarDireccionPaciente(int idDireccionPaciente) {
        for (int i = 0; i < listaDireccionesPaciente.size(); i++) {
            if (listaDireccionesPaciente.get(i).getIdDireccionPaciente() == idDireccionPaciente) {
                this.listaDireccionesPaciente.remove(i);
                JsonDb.save(Direcciones.class, listaDireccionesUsuarios);
                return true;
            }
        }
        return false;
    }

    @Override
    public DtoDireccion obtenerDireccionPaciente(int idDireccionPaciente) {
        DtoDireccion response = null;
        for (Direcciones listaDireccionesPacientes : listaDireccionesPaciente) {
            if (listaDireccionesPacientes.getIdDireccionUsuario() == idDireccionPaciente) {
                response = new DtoDireccion();
                response.setIdDireccionUsuario(listaDireccionesPacientes.getIdDireccionUsuario());
                response.setPais(listaDireccionesPacientes.getPais());
                response.setProvincia(listaDireccionesPacientes.getProvincia());
                response.setCiudad(listaDireccionesPacientes.getCiudad());
                response.setCalle(listaDireccionesPacientes.getCalle());
                response.setAltura(listaDireccionesPacientes.getAltura());
                response.setTelefono(listaDireccionesPacientes.getTelefono());

                return response;
            }
        }
        return null;
    }

    @Override
    public Boolean crearDireccionSucursal(DtoDireccion altaDireccionSucursal) {
        this.listaDireccionesSucursal.add(new Direcciones(
                altaDireccionSucursal.getIdDireccionSucursal(),
                altaDireccionSucursal.getPais(),
                altaDireccionSucursal.getProvincia(),
                altaDireccionSucursal.getCiudad(),
                altaDireccionSucursal.getCalle(),
                altaDireccionSucursal.getAltura(),
                altaDireccionSucursal.getTelefono()
        ));
        JsonDb.save(Direcciones.class, listaDireccionesSucursal);
        return true;
    }

    @Override
    public Boolean modificarDireccionSucursal(DtoDireccion modificarDireccionSucursal) {
        for (int i = 0; i < listaDireccionesSucursal.size(); i++) {
            if (listaDireccionesSucursal.get(i).getIdDireccionUsuario() == modificarDireccionSucursal.getIdDireccionSucursal()) {
                if (modificarDireccionSucursal.getPais() != null)
                    listaDireccionesSucursal.get(i).setPais(modificarDireccionSucursal.getPais());
                if (modificarDireccionSucursal.getProvincia() != null)
                    listaDireccionesSucursal.get(i).setProvincia(modificarDireccionSucursal.getProvincia());
                if (modificarDireccionSucursal.getCiudad() != null)
                    listaDireccionesSucursal.get(i).setCiudad(modificarDireccionSucursal.getCiudad());
                if (modificarDireccionSucursal.getCalle() != null)
                    listaDireccionesSucursal.get(i).setCalle(modificarDireccionSucursal.getCalle());
                if (modificarDireccionSucursal.getAltura() >= 0)
                    listaDireccionesSucursal.get(i).setAltura(modificarDireccionSucursal.getAltura());
                if (modificarDireccionSucursal.getTelefono() >= 0)
                    listaDireccionesSucursal.get(i).setTelefono(modificarDireccionSucursal.getTelefono());
                JsonDb.save(Direcciones.class, listaDireccionesUsuarios);
                return true;
            }
        }
        return false;
    }

    @Override
    public Boolean borrarDireccionSucursal(int idDireccionSucursal) {
        for (int i = 0; i < listaDireccionesSucursal.size(); i++) {
            if (listaDireccionesSucursal.get(i).getIdDireccionPaciente() == idDireccionSucursal) {
                this.listaDireccionesSucursal.remove(i);
                JsonDb.save(Direcciones.class, listaDireccionesUsuarios);
                return true;
            }
        }
        return false;
    }

    @Override
    public DtoDireccion obtenerDireccionSucursal(int idDireccionSucursal) {
        DtoDireccion response = null;
        for (Direcciones listaDireccionesSucursal : listaDireccionesSucursal) {
            if (listaDireccionesSucursal.getIdDireccionUsuario() == idDireccionSucursal) {
                response = new DtoDireccion();
                response.setIdDireccionUsuario(listaDireccionesSucursal.getIdDireccionUsuario());
                response.setPais(listaDireccionesSucursal.getPais());
                response.setProvincia(listaDireccionesSucursal.getProvincia());
                response.setCiudad(listaDireccionesSucursal.getCiudad());
                response.setCalle(listaDireccionesSucursal.getCalle());
                response.setAltura(listaDireccionesSucursal.getAltura());
                response.setTelefono(listaDireccionesSucursal.getTelefono());

                return response;
            }
        }
        return null;
    }
}

