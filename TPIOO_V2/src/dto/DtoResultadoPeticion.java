package dto;

import java.util.Date;

import model.Peticiones;
import model.Practicas;

public class DtoResultadoPeticion {

	private int idResultado;

	private String codPractica;

	private String valorResultado;

	private Date fechaFinalizada;

	// -- GETTER AND SETTER--//
	
	public int getIdResultado() {
		return idResultado;
	}

	public void setIdResultado(int idResultado) {
		this.idResultado = idResultado;
	}

	public String getCodPractica() {
		return codPractica;
	}

	public void setCodPractica(String codPractica) {
		this.codPractica = codPractica;
	}

	public String getValorResultado() {
		return valorResultado;
	}

	public void setValorResultado(String valorResultado) {
		this.valorResultado = valorResultado;
	}

	public Date getFechaFinalizada() {
		return fechaFinalizada;
	}

	public void setFechaFinalizada(Date fechaFinalizada) {
		this.fechaFinalizada = fechaFinalizada;
	}

}
