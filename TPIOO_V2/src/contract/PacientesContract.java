package contract;

import dto.DtoPaciente;
import model.ObraSociales;
import model.Sexo;

import java.util.Date;
import java.util.List;

public interface PacientesContract {
    Boolean save(DtoPaciente pacienteDTO);

    int calculateEdad(Date fechaNacimiento);

    int deletePaciente(long pacienteId);

    DtoPaciente getPacienteDNI(int dni);

    int getCodObraSociales(String nombreObraSocial);

    String getNombreObraSociales(int codObraSocial);

    List<DtoPaciente> allPacientes();

    Sexo[] getSexoString();

    List<ObraSociales> getObraSociales();

    DtoPaciente obtenerPaciente(long pacienteId);

    DtoPaciente obtenerPacienteXDni(int dni);
}
