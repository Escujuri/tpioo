package dto;

import model.Permisos;

import java.util.ArrayList;
import java.util.List;

public class DtoPermisos {
	
	private String permiso;
	
	private Boolean lectura;
	
	private Boolean escritura;

	// -- GETTER AND SETTER--//
	
	public String getPermiso() { return permiso; }

	public void setPermiso(String permiso) {
		this.permiso = permiso;
	}

	public Boolean getLectura() {
		return lectura;
	}

	public void setLectura(Boolean lectura) {
		this.lectura = lectura;
	}

	public Boolean getEscritura() {
		return escritura;
	}

	public void setEscritura(Boolean escritura) {
		this.escritura = escritura;
	}

	public List<Permisos> permisos = new ArrayList<Permisos>();

}
