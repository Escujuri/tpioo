package dto;

import java.util.List;

public class DtoPracticas {

	private String codPractica;

	private String nombrePractica;

	private int grupo;

	private int cantHorasResultado;

	private Boolean habilitado;

	private List<DtoAnalisis> analisisList;



	// -- GETTER AND SETTER--//
	
	public String getCodPractica() {
		return codPractica;
	}

	public void setCodPractica(String codPractica) {
		this.codPractica = codPractica;
	}

	public String getNombrePractica() {
		return nombrePractica;
	}

	public void setNombrePractica(String nombrePractica) {
		this.nombrePractica = nombrePractica;
	}

	public int getGrupo() {
		return grupo;
	}

	public void setGrupo(int grupo) {
		this.grupo = grupo;
	}

	public int getCantHorasResultado() {
		return cantHorasResultado;
	}

	public void setCantHorasResultado(int cantHorasResultado) {
		this.cantHorasResultado = cantHorasResultado;
	}

	public Boolean getHabilitado() {
		return habilitado;
	}

	public void setHabilitado(Boolean habilitado) {
		this.habilitado = habilitado;
	}

	public List<DtoAnalisis> getAnalisisList() {
		return analisisList;
	}

	public void setAnalisisList(List<DtoAnalisis> analisisList) {
		this.analisisList = analisisList;
	}

	public String toString() {
		return String.format(this.nombrePractica);
	}
}
