package view.sucursales;

import controller.ControllerDirecciones;
import controller.ControllerSucursales;
import dto.DtoDireccion;
import dto.DtoSucursales;
import view.BaseDialog;
import view.direcciones.CrearDireccion;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class CrearSucursalView extends BaseDialog {
    private JTextField txtNumSuc;
    private JTextField txtNomSuc;
    private JButton cancelButton;
    private JButton OKButton;
    public JPanel pnlCrearSucursales;
    private JCheckBox habilitadoCheckBox;
    private JButton borrarSucursalButton;
    private JButton btnDireccion;
    private JLabel lblDireccion;
    SucursalesView frame;
    CrearDireccion dialogForm;
    DtoDireccion direccion;
    private int sucursalId = -1;
    ControllerSucursales controllerSucursales = ControllerSucursales.getInstance();
    ControllerDirecciones controllerDirecciones = ControllerDirecciones.getInstance();

    public CrearSucursalView(SucursalesView parentFrame) {
        frame = parentFrame;
        setTitle("Crear Sucursales");
        setModal(true);
        setContentPane(pnlCrearSucursales);
        setSize(400, 450);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
        dialogForm = new CrearDireccion(this);
        setupButtons();
    }

    public void setEditing(int id){
        sucursalId = id;
        if (sucursalId != -1) {
            borrarSucursalButton.setEnabled(true);
            borrarSucursalButton.addActionListener(e ->borrarSucursal(id));
        }
        DtoSucursales dtoSucursales = controllerSucursales.obtenerSucursal(id);
        DtoDireccion dtoDireccion = controllerDirecciones.obtenerDireccionSucursal(id);
        txtNomSuc.setText(dtoSucursales.getNombreSucursal());
        txtNumSuc.setText(String.valueOf(dtoSucursales.getNumeroSucursal()));
        habilitadoCheckBox.setSelected(dtoSucursales.getHabilitado());
        try {
            lblDireccion.setText(dtoDireccion.getCalle() + " " + dtoDireccion.getAltura());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    private void setupButtons(){
        OKButton.addActionListener(e -> okPressed());
        btnDireccion.addActionListener(e -> showDireccion());
        cancelButton.addActionListener(e -> onCancel());
    }

    private void borrarSucursal(int id) {
        try {
            controllerSucursales.borrarSucursal(id);
            frame.dialogDisposed();

            JOptionPane.showMessageDialog(this, "La sucursal fue eliminado correctamente");
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, "Error: " + ex.getMessage());
            return;
        }
        dispose();
    }

    private void showDireccion() {
        dialogForm.setVisible(true);
    }

    @Override
    public void getDireccionInfo(DtoDireccion dire){
        System.out.println(" getDireccionInfo CrearSucursales");
        direccion = dire;
        lblDireccion.setText(dire.getCalle());
    }

    private Boolean validateInputUI () {
        Boolean response = false;

        if (txtNumSuc.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Ingrese un numero");
        }
        else if (txtNomSuc.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Ingrese un nombre");
        }
        else if (direccion == null) {
            JOptionPane.showMessageDialog(this, "Ingrese una dirrección");
        }
        else {
            response = true;
        }

        return response;
    }

    private void  okPressed() {
        DtoSucursales sucursales = new DtoSucursales();

        if (validateInputUI()) {
            try {
                sucursales.setNombreSucursal(txtNomSuc.getText());
                sucursales.setNumeroSucursal(Integer.parseInt(txtNumSuc.getText()));
                sucursales.setDireccion(direccion);
                sucursales.setHabilitado(habilitadoCheckBox.isSelected());

                if (controllerSucursales.existeSucursal(sucursales.getNumeroSucursal())) {
                    controllerSucursales.actualizarSucursal(sucursales);
                } else {
                    controllerSucursales.crearSucursal(sucursales);
                }

                frame.dialogDisposed();
                dispose();
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }

        }

    }

    private void onCancel(){
        dispose();
    }

}
