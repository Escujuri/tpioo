package view.peticiones;

import com.toedter.calendar.JDateChooser;
import controller.ControllerPacientes;
import controller.ControllerPeticiones;
import controller.ControllerPracticas;
import controller.ControllerSucursales;
import dto.*;
import view.BaseDialog;
import view.direcciones.CrearDireccion;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CrearPeticionesView extends BaseDialog {
    PeticionesView frame;
    private JPanel pnlCrearPeticion;
    private JTextField txtId;
    private JComboBox cmbPracticas;
    private JComboBox cmbPacientes;
    private JButton OKButton;
    private JButton cancelButton;
    private JButton btnBorrar;
    private JTable tblPracticas;
    private JPanel pnlPracticasList;
    private JPanel panelDate;
    private JComboBox cmbSucursal;
    private JLabel lblNombrePaciente;
    private DefaultComboBoxModel<DtoPaciente> cmboPacienteModel = new DefaultComboBoxModel();
    private DefaultComboBoxModel<DtoPracticas> cmboPracticasModel = new DefaultComboBoxModel();
    private DefaultComboBoxModel<DtoSucursales> cmboSucursalesModel = new DefaultComboBoxModel();
    private int peticionesId = -1;

    ControllerPacientes controllerPacientes = ControllerPacientes.getInstance();
    ControllerPracticas controllerPracticas = ControllerPracticas.getInstance();
    ControllerSucursales controllerSucursal = ControllerSucursales.getInstance();
    ControllerPeticiones controllerPeticiones = ControllerPeticiones.getInstance();

    DefaultTableModel tblPracticasModel;
    List<DtoPracticas> practicasList = new ArrayList<>();
    JDateChooser dateChooser = new JDateChooser();

    public CrearPeticionesView(PeticionesView parentFrame){
        frame = parentFrame;
        initView();
        setupButtons();
    }

    private void initView(){
        setTitle("Crear Peticion");
        setModal(true);
        setContentPane(pnlCrearPeticion);
        setSize(400, 450);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
        setupButtons();
        panelDate.add(dateChooser);

        DtoPracticas[] cmbPracticasOptions = controllerPracticas.getTodasPracticas().toArray(new DtoPracticas[0]);
        cmboPracticasModel = new DefaultComboBoxModel(cmbPracticasOptions);
        cmbPracticas.setModel(cmboPracticasModel);

        DtoSucursales[] cmbSucursalOptions = controllerSucursal.obtenerSucursales().toArray(new DtoSucursales[0]);
        cmboSucursalesModel = new DefaultComboBoxModel(cmbSucursalOptions);
        cmbSucursal.setModel(cmboSucursalesModel);

        DtoPaciente[] cmbPacienteOptions = controllerPacientes.obtenerPacientes().toArray(new DtoPaciente[0]);
        cmboPacienteModel = new DefaultComboBoxModel(cmbPacienteOptions);
        cmbPacientes.setModel(cmboPacienteModel);


        tblPracticasModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        tblPracticasModel.addColumn("");

        tblPracticas = new JTable(tblPracticasModel);

        pnlPracticasList.setLayout(new BorderLayout());
        pnlPracticasList.add(new JScrollPane(tblPracticas), BorderLayout.CENTER);
    }

    private void addToList(DtoPracticas practicas){
        if(!practicasList.contains(practicas)) {
            practicasList.add(practicas);
            tblPracticasModel.addRow(new Object[]{
                    practicas,
            });
        }
    }

    private void setupButtons(){
        OKButton.addActionListener(e -> onOkPressed());
        cancelButton.addActionListener(e -> onCancel());
        cmbPracticas.addActionListener(e -> addToList((DtoPracticas) cmbPracticas.getSelectedItem()));
        cmbPacientes.addActionListener(e -> actualizarNombrePaciente());
    }

    private void actualizarNombrePaciente() {
        try {
            DtoPaciente dtoPaciente = controllerPacientes.obtenerPacienteXDni(Integer.parseInt(cmbPacientes.getSelectedItem().toString()));
            lblNombrePaciente.setText(dtoPaciente.getNombre());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    private Boolean validateInputUI () {
        Boolean response = false;

        if (txtId.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Ingrese un numero");
        }
        else if (dateChooser.getDateFormatString().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Ingrese una fecha");
        }
        else {
            response = true;
        }

        return response;
    }

    private void onOkPressed() {
        DtoPeticion dtoPeticion = new DtoPeticion();

        if (validateInputUI()) {
            try {
                dtoPeticion.setIdPeticion(Integer.parseInt(txtId.getText()));
                dtoPeticion.setNombreSucursal(cmbSucursal.getSelectedItem().toString());
                dtoPeticion.setFechaCalculadaEntrega(dateChooser.getDate());
                Date date = new Date(System.currentTimeMillis());
                dtoPeticion.setFechaCarga(date);
                dtoPeticion.setPracticas(practicasList);
                DtoPaciente dtoPaciente = controllerPacientes.obtenerPacienteXDni(Integer.parseInt(cmbPacientes.getSelectedItem().toString()));
                dtoPeticion.setPacienteId(dtoPaciente.getPacienteId());
               if(dtoPaciente.getCodObraSociales() != -1){
                    dtoPeticion.setTieneObraSocial(true);
                    dtoPeticion.setSetCodObraSocial(dtoPaciente.getCodObraSociales());
                }else {
                    dtoPeticion.setTieneObraSocial(false);
                }

                if (controllerPeticiones.existePeticion(dtoPeticion.getIdPeticion())) {
                    controllerPeticiones.actualizarPeticion(dtoPeticion);
                } else {
                    controllerPeticiones.crearPeticion(dtoPeticion);
                }

                frame.dialogDisposed();
                dispose();
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    private void onCancel(){
        dispose();
    }

    public void setEditing(int id){
        peticionesId = id;
        if (peticionesId != -1) {
            btnBorrar.setEnabled(true);
            btnBorrar.addActionListener(e ->borrarPeticion(peticionesId));
        }

        try {
            DtoPeticion dtoPeticion = controllerPeticiones.obtenerPeticion(id);
            DtoPaciente dtoPaciente = controllerPacientes.obtenerPaciente(dtoPeticion.getIdPaciente());
            DtoSucursales dtoSucursales = controllerSucursal.obtenerSucursal(controllerSucursal.obtenerIdSucursalByNombre(dtoPeticion.getNombreSucursal()));

            txtId.setText(String.valueOf(dtoPeticion.getIdPeticion()));
            cmboPacienteModel.setSelectedItem(dtoPaciente.getDni());
            cmboSucursalesModel.setSelectedItem(dtoSucursales.getNombreSucursal());
            dateChooser.setDate(dtoPeticion.getFechaCalculadaEntrega());

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    private void borrarPeticion(int peticionesId) {
        try {
            controllerPeticiones.eliminarPeticion(peticionesId);
            frame.dialogDisposed();

            JOptionPane.showMessageDialog(this, "La peticion fue eliminado correctamente");
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, "Error: " + ex.getMessage());
            return;
        }
        dispose();
    }


}
