package contract;

import dto.DtoPermisos;
import dto.DtoRoles;
import model.Roles;

import java.util.List;

public interface RolesContract {

    Boolean crearRol(DtoRoles Roles);

    Boolean modificarRol(DtoRoles Roles);

    Boolean borrarRol(String nombreRol);

    List<DtoRoles> obtenerRoles();
    void setearRolActual(DtoRoles dtoRole);

    String obtenerRolActual();

}
