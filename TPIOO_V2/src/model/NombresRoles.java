package model;

public enum NombresRoles {
    Recepcionista("Recepcionista"),
    Laboratorista("Laboratorista"),
    Administrador("Administrador");

    private String descripcion;

    NombresRoles(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    @Override
    public String toString() {
        return this.descripcion;
    }
}