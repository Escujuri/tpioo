package dto;

public class DtoRoles {

    private String nombreRol;

    private DtoPermisos permisos;

    public String getNombreRol() {
        return nombreRol;
    }

    public void setNombreRol(String nombreRol) {
        this.nombreRol = nombreRol;
    }

    public DtoPermisos getPermisos() {
        return permisos;
    }

    public void setPermisos(DtoPermisos permisos) {
        this.permisos = permisos;
    }

    public String toString() {
        return String.format(this.nombreRol);
    }
}