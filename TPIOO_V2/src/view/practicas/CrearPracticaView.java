package view.practicas;


import controller.ControllerAnalisis;
import controller.ControllerPacientes;
import controller.ControllerPracticas;
import dto.DtoAnalisis;
import dto.DtoPracticas;
import view.BaseDialog;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

public class CrearPracticaView extends BaseDialog {
    public JPanel pnlCrearPractica;
    private JTextField txtCode;
    private JTextField txtNombre;
    private JTextField txtGrupo;
    private JLabel lblCod;
    private JComboBox cmboAnalisis;
    private JTable tblAnalisis;
    private JTextField txtTiempoResultado;
    private JButton cancelButton;
    private JButton okButton;
    private JButton borrarPracticaButton;
    private JCheckBox habilitadoCheckBox;
    private JPanel pnlListAnalisis;
    PracticasView frame;
    ControllerPracticas controllerPracticas = ControllerPracticas.getInstance();
    ControllerAnalisis controllerAnalisis = ControllerAnalisis.getInstance();
    ControllerPacientes controllerPacientes = ControllerPacientes.getInstance();
    private DefaultComboBoxModel<DtoAnalisis> cmboAnalisisModel = new DefaultComboBoxModel();
    DefaultTableModel tblAnalisisModel;
    List<DtoAnalisis> analisisList = new ArrayList<>();

    public CrearPracticaView(PracticasView parentFrame){
        frame = parentFrame;
        initView();
        setupButtons();
    }


    private void initView() {
        setTitle("Crear practica");
        setModal(true);
        setContentPane(pnlCrearPractica);
        setSize(450, 450);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });


        DtoAnalisis[] cmbAnalisiOptions = controllerAnalisis.getTodosAnalisis().toArray(new DtoAnalisis[0]);
        cmboAnalisisModel = new DefaultComboBoxModel(cmbAnalisiOptions);
        cmboAnalisis.setModel(cmboAnalisisModel);

        tblAnalisisModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tblAnalisisModel.addColumn("");
        tblAnalisis = new JTable(tblAnalisisModel);
        pnlListAnalisis.setLayout(new BorderLayout());
        pnlListAnalisis.add(new JScrollPane(tblAnalisis), BorderLayout.CENTER);
    }

    private void addToList(DtoAnalisis analisis){
        if(!analisisList.contains(analisis)) {
            analisisList.add(analisis);
            tblAnalisisModel.addRow(new Object[]{
                    analisis,
            });
        }
    }

    private void setupButtons(){
        okButton.addActionListener(e -> okPressed());
        cancelButton.addActionListener(e -> onCancel());
        cmboAnalisis.addActionListener(e -> addToList((DtoAnalisis) cmboAnalisis.getSelectedItem()));
    }

    private void  okPressed() {
        DtoPracticas practicas = new DtoPracticas();
        practicas.setNombrePractica(txtNombre.getText());
        practicas.setCodPractica(txtCode.getText());
        practicas.setHabilitado(habilitadoCheckBox.isSelected());
        practicas.setCantHorasResultado(1234);
        practicas.setAnalisisList(analisisList);
        controllerPracticas.crearPractica(practicas);
        frame.dialogDisposed();
        dispose();
    }

    public void setEditing(String id) {
        borrarPracticaButton.setEnabled(true);
        borrarPracticaButton.addActionListener(e -> desHabilitarPractica(id));
        DtoPracticas dto = controllerPracticas.getPracticaById(id);
        txtNombre.setText(dto.getNombrePractica());
        txtCode.setText(id);
        txtGrupo.setText(String.valueOf((dto.getGrupo())));
        habilitadoCheckBox.setSelected(dto.getHabilitado());
        txtTiempoResultado.setText(String.valueOf(dto.getCantHorasResultado()));
        cmboAnalisis.setEnabled(false);
        List<DtoAnalisis> analisisList = dto.getAnalisisList();
        if(analisisList != null) {
            for (DtoAnalisis analisis : analisisList) {
                tblAnalisisModel.addRow(new Object[]{
                        analisis,
                });
            }
        } else {
            System.out.println("lista nula???");
        }
    }

    private void desHabilitarPractica(String id) {
        try {
            controllerPracticas.inhabilitarPractica(id);
            frame.dialogDisposed();

            JOptionPane.showMessageDialog(this, "La practica: " + id + ", fue deshabilitado correctamente");
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, "Error2: " + ex.getMessage());
            return;
        }
        dispose();

    }

    private void onCancel() {
        dispose();
    }
}
