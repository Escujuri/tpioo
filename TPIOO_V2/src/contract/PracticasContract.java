package contract;

import dto.DtoAnalisis;
import dto.DtoPracticas;
import model.Analisis;

import java.util.List;

public interface PracticasContract {

    Boolean crearPractica(DtoPracticas Practicas);

    Boolean actualizarPractica(DtoPracticas Roles);

    Boolean inhabilitarPractica(String codPractica);

    DtoPracticas getPracticaById(String codPractica);

    Boolean existeValoresCriticos(String codPractica);

    Boolean existeValoresReservados(String codPractica);

    List<DtoAnalisis> getTodosAnalisis();

    Analisis getAnalisisById(String idAnalisis);

    List<DtoPracticas> getTodasPracticas();

}
