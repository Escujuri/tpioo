package dto;

public class DtoDireccion {

	private int idDireccionUsuario;

	private int idDireccionPaciente;

	private int idDireccionSucursal;
	
	private String pais;
	
	private String provincia;
	
	private String ciudad;
	
	private String calle;

	private int altura;
	
	private int telefono;

	public DtoDireccion() {}


	//-- GETTER AND SETTER--//

	public int getIdDireccionUsuario() {
		return idDireccionUsuario;
	}

	public void setIdDireccionUsuario(int idDireccionUsuario) {
		this.idDireccionUsuario = idDireccionUsuario;
	}

	public int getIdDireccionPaciente() {
		return idDireccionPaciente;
	}

	public void setIdDireccionPaciente(int idDireccionPaciente) {
		this.idDireccionPaciente = idDireccionPaciente;
	}

	public int getIdDireccionSucursal() {
		return idDireccionSucursal;
	}

	public void setIdDireccionSucursal(int idDireccionSucursal) {
		this.idDireccionSucursal = idDireccionSucursal;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public int getAltura() {
		return altura;
	}

	public void setAltura(int altura) {
		this.altura = altura;
	}

	public int getTelefono() {
		return telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}
}
