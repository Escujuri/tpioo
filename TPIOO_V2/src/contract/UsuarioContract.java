package contract;


import dto.DtoUsuarios;

import java.util.List;

public interface UsuarioContract {

    Boolean crearUsuario(DtoUsuarios nuevoUsuario);

    Boolean asignarRol(int dni, String Rol);

    Boolean modificarUsuario(DtoUsuarios modificarUsuario);

    Boolean borrarUsuario(int dni);

    List<DtoUsuarios> allUsuarios();

    DtoUsuarios  obtenerUsuario(int id);

    Boolean existeUsuario(int dni);
}
