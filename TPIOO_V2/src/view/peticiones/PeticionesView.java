package view.peticiones;

import controller.ControllerPacientes;
import controller.ControllerPeticiones;
import controller.ControllerSeguridad;
import dto.DtoPaciente;
import dto.DtoPeticion;
import utils.Constants;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;


public class PeticionesView {
    private JButton crearPeticionButton;
    private JTable tblPeticiones;
    public JPanel pnlPeticiones;
    private JPanel pnlPeticionesList;
    private JButton btnActualizar;
    private DefaultTableModel tblPeticionesModel;
    CrearPeticionesView dialogForm;
    ResultadoPeticion dialogResultPet;
    ControllerPeticiones controllerPeticiones = ControllerPeticiones.getInstance();
    ControllerPacientes controllerPacientes = ControllerPacientes.getInstance();
    ControllerSeguridad controllerSeguridad = ControllerSeguridad.getInstance();

    public PeticionesView() {
        initPeticiones();
        setupButtons();
        loadPeticiones();
    }

    private void initPeticiones(){
        dialogForm = new CrearPeticionesView(this);

        tblPeticionesModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        tblPeticionesModel.addColumn("Id");
        tblPeticionesModel.addColumn("Asignado A");
        tblPeticionesModel.addColumn("Sucursal");
        tblPeticionesModel.addColumn("Fecha de entrega");
        tblPeticionesModel.addColumn("Fecha de carga");


        tblPeticiones = new JTable(tblPeticionesModel);

        tblPeticiones.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                if (me.getClickCount() == 2) {
                    if (!controllerSeguridad.obtenerRolActual().equals(Constants.RECEPCIONISTA)) {
                        int row = tblPeticiones.getSelectedRow();
                        int idSucursal = (int) tblPeticiones.getModel().getValueAt(row, 0);
                        showEditPeticiones(idSucursal);
                    } else {
                        JOptionPane.showMessageDialog(null, "Su rol no adminte esta operacion");
                    }
                }
            }
        });

        pnlPeticionesList.setLayout(new BorderLayout());
        pnlPeticionesList.add(new JScrollPane(tblPeticiones), BorderLayout.CENTER);
    }

    private void setupButtons(){
        crearPeticionButton.addActionListener(e-> showCrearPeticiones());
        btnActualizar.addActionListener(e-> loadPeticiones());
    }

    void showCrearPeticiones(){
        dialogForm = new CrearPeticionesView(this);
        dialogForm.setVisible(true);
    }

    void showEditPeticiones(int id) {
      //  dialogResultPet = new ResultadoPeticion(id);
     //   dialogResultPet.setVisible(true);
       dialogForm.setEditing(id);
      dialogForm.setVisible(true);
    }

    public void dialogDisposed(){
        loadPeticiones();
    }

    public  void loadPeticiones() {
        List<DtoPeticion> peticiones = controllerPeticiones.obtenerPeticiones();
        tblPeticionesModel.setRowCount(0);
        for (DtoPeticion dto : peticiones) {
          DtoPaciente paciente = controllerPacientes.obtenerPaciente(dto.getIdPaciente());
          try {
              tblPeticionesModel.addRow(new Object[]{
                      dto.getIdPeticion(),
                      paciente.getNombre(),
                      dto.getNombreSucursal(),
                      dto.getFechaCalculadaEntrega(),
                      dto.getFechaCarga()
              });
          } catch  (Exception ex) {
              System.out.println(ex.getMessage());
          }

        }
    }

}
