package view;

import controller.ControllerSeguridad;
import dto.DtoRoles;
import utils.Constants;
import utils.JsonDb;

import javax.swing.*;
import java.util.List;

public class Login extends JFrame{

    private JComboBox cmboRoles;
    private JButton ingresarButton;
    private JPanel pnlLogin;
    private JTextField textField1;
    private JTextField textField2;
    JFrame frame = new JFrame("Login");
    ControllerSeguridad controllerSeguridad = ControllerSeguridad.getInstance();
    private DefaultComboBoxModel<DtoRoles> cmboRolesModel = new DefaultComboBoxModel();

    public Login() {
        initView();
        setupButtons();
        loadRoles();
    }

    void initView() {
        frame.setSize(400, 300);
        frame.setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setContentPane(pnlLogin);
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        DtoRoles dtoRol = new DtoRoles();
        dtoRol.setNombreRol(Constants.RECEPCIONISTA);
        controllerSeguridad.setearRolActual(dtoRol);
    }

   private void loadRoles() {
       List<DtoRoles> roles =  controllerSeguridad.obtenerRoles();

       DtoRoles[] cmbRolesOpt = controllerSeguridad.obtenerRoles().toArray(new DtoRoles[0]);
       cmboRolesModel = new DefaultComboBoxModel(cmbRolesOpt);
       cmboRoles.setModel(cmboRolesModel);
   }

   private void setupButtons() {
       ingresarButton.addActionListener(e-> ingresar());
       cmboRoles.addActionListener( e->setRol((DtoRoles) cmboRoles.getSelectedItem()) );
    }

    private void setRol(DtoRoles rol) {
        controllerSeguridad.setearRolActual(rol);
    }

    private void ingresar(){

        MainForm mainForm = new MainForm();
        frame.dispose();
    }

    private void onCancel(){
        frame.dispose();
    }
}
