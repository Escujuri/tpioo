package controller;

import contract.ResuladoPeticionesContract;
import dto.DtoResultadoPeticion;
import model.ResultadoPeticion;

import java.util.List;

public class ControllerResultadoPeticion {

    private static ControllerResultadoPeticion cp = null;

    ResuladoPeticionesContract resuladoPeticionesContract = new ResultadoPeticion();

    public static synchronized ControllerResultadoPeticion getInstance() {
        if (cp == null) {
            cp = new ControllerResultadoPeticion();
        }
        return cp;
    }

    public void crearResultadoPeticion(DtoResultadoPeticion resultadoPeticion) {resuladoPeticionesContract.crearResultadoPeticion(resultadoPeticion);}

    public void modificarResultadoPeticion(DtoResultadoPeticion resultadoPeticion) { resuladoPeticionesContract.actualizarResultadoPeticion(resultadoPeticion); }

    public void eliminarResultadoPeticion(int idResultado) { resuladoPeticionesContract.eliminarResultadoPeticion(idResultado); }

    public List<DtoResultadoPeticion> obtenerTodosResultadoPeticion() { return resuladoPeticionesContract.obtenerTodosResultadoPeticion(); }

    public DtoResultadoPeticion obtenerResultadoPeticion(int idResultado) { return (DtoResultadoPeticion) resuladoPeticionesContract.obtenerResultadoPeticion(idResultado);}

}