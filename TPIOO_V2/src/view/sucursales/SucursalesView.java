package view.sucursales;

import controller.ControllerSucursales;
import dto.DtoSucursales;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

public class SucursalesView {
    public JPanel pnlSucursales;
    private JButton crearSucursalButton;
    private JTable tblSucursales;
    private JPanel pnlListadoSucursale;
    private DefaultTableModel tblSucursalesModel;
    CrearSucursalView dialogForm;
    ControllerSucursales controllerSucursales =  ControllerSucursales.getInstance();

    public SucursalesView(){
        initSucursales();
        setupButtons();
        loadSucursales();
    }


    private void initSucursales(){
        dialogForm = new CrearSucursalView(this);

        tblSucursalesModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        tblSucursalesModel.addColumn("Numero");
        tblSucursalesModel.addColumn("Nombre");
        tblSucursalesModel.addColumn("Habilitada");

        tblSucursales = new JTable(tblSucursalesModel);

        tblSucursales.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                if (me.getClickCount() == 2) {
                    int row = tblSucursales.getSelectedRow();
                    int idSucursal = (int)tblSucursales.getModel().getValueAt(row, 0);
                    showEditSucursales(idSucursal);
                }
            }
        });

        pnlListadoSucursale.setLayout(new BorderLayout());
        pnlListadoSucursale.add(new JScrollPane(tblSucursales), BorderLayout.CENTER);
    }

    private void setupButtons(){
        crearSucursalButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showCrearSucursales();
            }
        });


    }

    void showCrearSucursales(){
        dialogForm = new CrearSucursalView(this);
        dialogForm.setVisible(true);
    }

    void showEditSucursales(int id){
        dialogForm = new CrearSucursalView(this);
        dialogForm.setEditing(id);
        dialogForm.setVisible(true);
    }

    public void dialogDisposed(){
        System.out.println("getInfo");
        loadSucursales();
    }

    public  void loadSucursales() {
        List<DtoSucursales> sucursales =  controllerSucursales.obtenerSucursales();

        tblSucursalesModel.setRowCount(0);

        for (DtoSucursales dto: sucursales) {
            tblSucursalesModel.addRow(new Object[] {
                    dto.getNumeroSucursal(),
                    dto.getNombreSucursal(),
                    dto.getHabilitado()
            });
        }

}
}
