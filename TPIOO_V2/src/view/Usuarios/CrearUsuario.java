package view.Usuarios;

import com.toedter.calendar.JDateChooser;
import controller.ControllerUsuarios;
import dto.DtoDireccion;
import dto.DtoSucursales;
import dto.DtoUsuarios;
import view.BaseDialog;
import view.direcciones.CrearDireccion;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class CrearUsuario extends BaseDialog {
    private JTextField txtDni;
    private JTextField txtNombre;
    private JTextField txtEmail;
    private JTextField txtContraseña;
    private JCheckBox habilitadoCheckBox;
    private JButton cancelButton;
    private JButton okButton;
    private JLabel jLabel;
    private JPanel pnlCrearUsuario;
    private JButton btnDireccion;
    private JLabel lblDireccion;
    private JButton borrarUsuarioButton;
    private JPanel panelDate;
    private UsuariosView frame;
    CrearDireccion dialogForm;
    DtoDireccion direccion;
    ControllerUsuarios controllerUsuarios =  ControllerUsuarios.getInstance();
    JDateChooser dateChooser = new JDateChooser();

    public CrearUsuario(UsuariosView parentFrame){
    frame = parentFrame;
    setTitle("Crear usuario");
    setModal(true);
    setContentPane(pnlCrearUsuario);
    setSize(400, 450);
    setLocationRelativeTo(null);
    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    dialogForm = new CrearDireccion(this);
    addWindowListener(new WindowAdapter() {
        public void windowClosing(WindowEvent e) {
            onCancel();
        }
    });
    panelDate.add(dateChooser);
    setupButtons();
}

    private void onCancel(){
       dispose();
    }

   @Override
    public void getDireccionInfo(DtoDireccion dire){
        System.out.println("getDireccionInfo en CrearUsuario");
        direccion = dire;
        lblDireccion.setText(dire.getCalle());
    }

    private void setupButtons(){
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                okPressed();
            }
        });
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        btnDireccion.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialogForm.setVisible(true);
            }
        });
    }

    private Boolean validateInputUI () {
        Boolean response = false;

        if (txtDni.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Ingrese un DNI");
        }
        else if (txtNombre.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Ingrese un Nombre");
        }
        else if (txtEmail.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Ingrese un Email");
        }
        else if (txtContraseña.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Ingrese una contraseña");
        }
        else if (dateChooser.getDateFormatString().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Ingrese una fecha de nacimiento");
        }
        else if(direccion == null){
            JOptionPane.showMessageDialog(this, "Ingrese una direccion");
        } else {
            response = true;
        }

        return response;
    }

    private void okPressed() {
        DtoUsuarios usuario = new DtoUsuarios();

        if (validateInputUI()) {
            try {
                usuario.setDni(Integer.parseInt(txtDni.getText()));
                usuario.setNombreUsuario(txtNombre.getText());
                usuario.setEmail(txtEmail.getText());
                usuario.setContrasenia(txtContraseña.getText());
                usuario.setHabilitado(habilitadoCheckBox.isSelected());
                usuario.setFechaNacimiento(dateChooser.getDate());
                usuario.setDireccion(direccion);

                if (controllerUsuarios.existeUsuario(usuario.getDni())) {
                    System.out.println("Ingreso a modificarUsuario");
                    controllerUsuarios.modificarUsuario(usuario);
                } else {
                    controllerUsuarios.crearUsuario(usuario);
                    System.out.println("Ingreso a crear usuario");
                }

                frame.dialogDisposd();
                dispose();
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }

    }

    public void setEditing(int id){
        if (id != -1) {
            borrarUsuarioButton.setEnabled(true);
            borrarUsuarioButton.addActionListener(e ->borrarUsuario(id));
        }

        DtoUsuarios dto = controllerUsuarios.obtenerUsuario(id);

        try {
            txtDni.setText(String.valueOf(dto.getDni()));
            txtNombre.setText(dto.getNombreUsuario());
            txtEmail.setText(dto.getEmail());
            habilitadoCheckBox.setSelected(dto.getHabilitado());
            dateChooser.setDate(dto.getFechaNacimiento());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    private void borrarUsuario(int id) {
        try {
            controllerUsuarios.borrarUsuario(id);
            frame.dialogDisposd();
            JOptionPane.showMessageDialog(this, "El usuario: " + id + ", fue eliminado correctamente");
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, "Error2: " + ex.getMessage());
            return;
        }
        dispose();
    }

}
