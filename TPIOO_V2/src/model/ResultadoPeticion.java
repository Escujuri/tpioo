package model;

import contract.ResuladoPeticionesContract;
import dto.DtoResultadoPeticion;
import utils.JsonDb;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class ResultadoPeticion implements ResuladoPeticionesContract {

    private int idResultado;
    private String codPractica;
    private String valorResultado;
    private Date fechaFinalizada;
    private static int numeradorResultado = 0;

    private static List<ResultadoPeticion> listaResultadoPeticion;

    static {
        listaResultadoPeticion = utils.JsonDb.getAll(ResultadoPeticion.class);
    }

    public ResultadoPeticion() {
    }

    public ResultadoPeticion(int idResultado, String codPractica, String valorResultado, Date fechaFinalizada) {
        numeradorResultado ++;
        this.idResultado = numeradorResultado;
        this.codPractica = codPractica;
        this.valorResultado = valorResultado;
        this.fechaFinalizada = fechaFinalizada;
    }

    @Override
    public Boolean crearResultadoPeticion(DtoResultadoPeticion resultadoPeticion) {
        listaResultadoPeticion.add(new ResultadoPeticion(resultadoPeticion.getIdResultado(),resultadoPeticion.getCodPractica(),resultadoPeticion.getValorResultado(),resultadoPeticion.getFechaFinalizada()));
        JsonDb.save(ResultadoPeticion.class, listaResultadoPeticion);

        return true;
    }

    @Override
    public Boolean eliminarResultadoPeticion(int idResultado) {
        for (int i = 0; i < listaResultadoPeticion.size(); i++){
            if (listaResultadoPeticion.get(i).getIdResultado() == idResultado) {
                listaResultadoPeticion.remove(i);
                JsonDb.save(ResultadoPeticion.class,listaResultadoPeticion);
                return true;
            }
        }
        return false;
    }

    @Override
    public Boolean actualizarResultadoPeticion(DtoResultadoPeticion peticion) {
        if (this.existeResultadoPeticion(idResultado)){
            for (int i = 0; i < listaResultadoPeticion.size();i++){
                if(listaResultadoPeticion.get(i).getIdResultado() == idResultado){
                    listaResultadoPeticion.get(i).idResultado = idResultado;
                    listaResultadoPeticion.get(i).codPractica = codPractica;
                    listaResultadoPeticion.get(i).valorResultado = valorResultado;
                    listaResultadoPeticion.get(i).fechaFinalizada = fechaFinalizada;
                    JsonDb.save(ResultadoPeticion.class, listaResultadoPeticion);
                    return true;
                }
            }
        }
        return null;
    }

    @Override
    public List<DtoResultadoPeticion> obtenerTodosResultadoPeticion() {
        DtoResultadoPeticion object = null;
        List<DtoResultadoPeticion> listaResultadoPeticionResponse = new ArrayList<DtoResultadoPeticion>();
        for (int i = 0; i < listaResultadoPeticion.size(); i++) {
            object = new DtoResultadoPeticion();
            object.setIdResultado(listaResultadoPeticion.get(i).getIdResultado());
            object.setCodPractica(listaResultadoPeticion.get(i).getCodPractica());
            object.setValorResultado(listaResultadoPeticion.get(i).getValorResultado());
            object.setFechaFinalizada(listaResultadoPeticion.get(i).getFechaFinalizada());
            listaResultadoPeticionResponse.add(object);
            JsonDb.save(ResultadoPeticion.class, listaResultadoPeticion);
        }

        return null;
    }

    @Override
    public List<DtoResultadoPeticion> obtenerResultadoPeticion(int idResultado) {
        DtoResultadoPeticion object;
        for (int i = 0; i < listaResultadoPeticion.size(); i++){
            if (listaResultadoPeticion.get(i).getIdResultado() == idResultado){
                object = new DtoResultadoPeticion();
                object.setIdResultado(listaResultadoPeticion.get(i).getIdResultado());
                object.setCodPractica(listaResultadoPeticion.get(i).getCodPractica());
                object.setValorResultado(listaResultadoPeticion.get(i).getValorResultado());
                object.setFechaFinalizada(listaResultadoPeticion.get(i).getFechaFinalizada());
                return (List<DtoResultadoPeticion>) object;
            }
        }
        return null;
    }

    private Boolean existeResultadoPeticion(int idResultado) {
        for (int i = 0; i < listaResultadoPeticion.size(); i++) {
            if (Objects.equals(listaResultadoPeticion.get(i).getIdResultado(), idResultado)) {
                return true;
            }
        }
        return false;
    }

    // -- GETTER AND SETTER--//
    public int getIdResultado() { return idResultado; }
    public void setIdResultado(int idResultado) {this.idResultado = idResultado;}
    public String getCodPractica() {return codPractica;}
    public void setCodPractica(String codPractica) {this.codPractica = codPractica;}
    public String getValorResultado() {return valorResultado;}
    public void setValorResultado(String valorResultado) {this.valorResultado = valorResultado;}
    public Date getFechaFinalizada() {return fechaFinalizada;}
    public void setFechaFinalizada(Date fechaFinalizada) { this.fechaFinalizada = fechaFinalizada; }
}
