package controller;

import contract.AnalisisContract;
import dto.DtoAnalisis;
import model.Analisis;

import java.util.List;

public class ControllerAnalisis {

    private static ControllerAnalisis cu = null;

    private AnalisisContract analisisContract = new Analisis();

    public static synchronized ControllerAnalisis getInstance() {
        if (cu == null) {
            cu = new ControllerAnalisis();
        }
        return cu;
    }

    public void crearAnalisis(DtoAnalisis nuevoAnalisis) {
        analisisContract.crearAnalisis(nuevoAnalisis);
    }

    public void modificarAnalisis(DtoAnalisis modificarAnaliss) {
        analisisContract.editarAnalisis(modificarAnaliss);
    }

    public List<DtoAnalisis> getTodosAnalisis () {
        return analisisContract.getTodosAnalisis();
    }

    public void borrarAnalisis(int idAnalisis) {
        analisisContract.borrarAnalisis(idAnalisis);
    }

    public DtoAnalisis obtenerAnalisis(int idAnalisis) {
        return analisisContract.obtenerAnalisis(idAnalisis);
    }
}
