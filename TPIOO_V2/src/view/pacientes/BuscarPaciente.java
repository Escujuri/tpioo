package view.pacientes;

import controller.ControllerPacientes;
import dto.DtoAnalisis;
import view.BaseDialog;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class BuscarPaciente extends BaseDialog  {
    private JTextField txtDNI;
    private JButton btnCancel;
    private JButton btnOK;
    private JPanel pnlBuscarPaciente;
    private JLabel lblDNI;
    ControllerPacientes controllerPacientes = ControllerPacientes.getInstance();

    public BuscarPaciente(long pacienteId) {
        initView();
        setupButtons();
    }

    private void initView() {

        setTitle(" Buescar Pacientes");
        setModal(true);
        setContentPane(pnlBuscarPaciente);
        setSize(400, 450);
        setLocationRelativeTo(null);
        getRootPane().setDefaultButton(btnOK);

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

    }

    private void setupButtons(){
        btnOK.addActionListener(e -> okPressed());
        btnCancel.addActionListener(e -> onCancel());
    }

    private void okPressed() {
        try {
            controllerPacientes.obtenerPacienteXDni(Integer.parseInt(txtDNI.getSelectedText()));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    private void onCancel(){
        dispose();
    }
}
