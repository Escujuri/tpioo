package view.peticiones;

import controller.ControllerPacientes;
import controller.ControllerPeticiones;
import controller.ControllerSucursales;
import dto.*;
import view.BaseDialog;
import view.analisis.CrearAnalisiView;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class ResultadoPeticion extends BaseDialog {
    private JButton okButton;
    private JButton cancelButton;
    private JComboBox cmboPracticas;
    private JButton editarButton;
    private JTable tblAnalisis;
    private JPanel pnlModificarPerticion;
    private JLabel lblSucursal;
    private JLabel lblPaciente;
    private JLabel lblPeticion;
    private JPanel pnlAnalisisList;
    private DefaultTableModel tblAnalisisModel;

    ControllerPeticiones controllerPeticiones = ControllerPeticiones.getInstance();
    ControllerPacientes controllerPacientes = ControllerPacientes.getInstance();
    ControllerSucursales controllerSucursal = ControllerSucursales.getInstance();
    private DefaultComboBoxModel<DtoPracticas> cmboPracticasModel = new DefaultComboBoxModel();

    public ResultadoPeticion(int idPeticion) {
        initView();
        setupViewPetcion(idPeticion);
    }

    private void initView(){
        setTitle("Modificar resultado de peticion");
        setModal(true);
        setContentPane(pnlModificarPerticion);
        setSize(500, 500);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        tblAnalisisModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tblAnalisisModel.addColumn("id");
        tblAnalisisModel.addColumn("Nombre analisis");

        tblAnalisis = new JTable(tblAnalisisModel);

        pnlAnalisisList.setLayout(new BorderLayout());
        pnlAnalisisList.add(new JScrollPane(tblAnalisis), BorderLayout.CENTER);



        setupButtons();
    }

    private void  setupViewPetcion(int id){
        try {
            DtoPeticion dtoPeticion = controllerPeticiones.obtenerPeticion(id);
            DtoPaciente dtoPaciente = controllerPacientes.obtenerPaciente(dtoPeticion.getIdPaciente());
            lblPaciente.setText(dtoPaciente.getNombre());
            lblPeticion.setText(String.valueOf(dtoPeticion.getIdPeticion()));
            lblSucursal.setText(dtoPeticion.getNombreSucursal());

           DtoPracticas[] cmbPracticasOptions = dtoPeticion.getPracticas().toArray(new DtoPracticas[0]);
           cmboPracticasModel = new DefaultComboBoxModel(cmbPracticasOptions);
           cmboPracticas.setModel(cmboPracticasModel);
           cmboPracticas.addActionListener(e -> addToList((DtoPracticas) cmboPracticas.getSelectedItem()));


        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    private void addToList(DtoPracticas dtoPracticas){
        List<DtoAnalisis> analisisList = dtoPracticas.getAnalisisList();
        tblAnalisisModel.setRowCount(0);
        for (DtoAnalisis dto : analisisList) {
            try {
                tblAnalisisModel.addRow(new Object[]{
                        dto.getIdAnalisis(),
                        dto.getNombreAnalisis(),
                });
            } catch  (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }


    private void setupButtons(){

        tblAnalisis.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                if (me.getClickCount() == 2) {
                    CrearAnalisiView dialogForm = new CrearAnalisiView();
                    int row = tblAnalisis.getSelectedRow();
                    int idAnalisis = (int)tblAnalisis.getModel().getValueAt(row, 0);
                    dialogForm.setEditing(idAnalisis);
                    dialogForm.setVisible(true);
                }
            }
        });
    }

    private void onCancel(){
        dispose();
    }
}
