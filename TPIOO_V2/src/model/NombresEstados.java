package model;

public enum NombresEstados {
    Pendiente("Pendiente"),
    Parcial("Parcial"),
    Finalizado("Finalizado");

    private String descripcion;

    NombresEstados(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    @Override
    public String toString() {
        return this.descripcion;
    }
}