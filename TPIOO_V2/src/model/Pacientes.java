package model;

import contract.PacientesContract;
import dto.DtoPaciente;
import dto.DtoPeticion;
import utils.IdGenerator;
import utils.JsonDb;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Pacientes implements PacientesContract {

    private long pacienteId;
    private int dni;
    private String nombre;
    private String email;
    private int codObraSociales;
    private String sexo;
    private Date fechaNacimiento;
    private Boolean habilitado;
    private Peticiones peticiones = new Peticiones();
    private static List<Pacientes> _listaPacientes;
    private static List<ObraSociales> _obrasSociales;

    static {
        _obrasSociales = utils.JsonDb.getAll(ObraSociales.class);
        _listaPacientes = utils.JsonDb.getAll(Pacientes.class);
    }

    // -- GETTER AND SETTER--//

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public Boolean getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(Boolean habilitado) {
        this.habilitado = habilitado;
    }

    public long getPacienteId() {
        return pacienteId;
    }

    public void setPacienteId(long pacienteId) {
        this.pacienteId = pacienteId;
    }

    public int getCodObraSociales() {
        return codObraSociales;
    }

    public void setCodObraSociales(int codObraSociales) {
        this.codObraSociales = codObraSociales;
    }


    @Override
    public List<DtoPaciente> allPacientes() {
        DtoPaciente object = null;
        List<DtoPaciente> listaPacienteResponse = new ArrayList<DtoPaciente>();

        for (Pacientes listaPaciente : _listaPacientes) {
            object = new DtoPaciente();
            object.setDni(listaPaciente.getDni());
            object.setEmail(listaPaciente.getEmail());
            object.setFechaNacimiento(listaPaciente.getFechaNacimiento());
            object.setHabilitado(listaPaciente.getHabilitado());
            object.setSexo(listaPaciente.getSexo());
            object.setNombre(listaPaciente.getNombre());
            object.setPacienteId(listaPaciente.getPacienteId());
            object.setCodObraSociales(listaPaciente.getCodObraSociales());

            listaPacienteResponse.add(object);
        }
        return listaPacienteResponse;
    }

    public Pacientes() {
    }

    public Pacientes(int dni, String nombre, String email, Boolean habilitado, Date fechaNacimiento, String sexo, long pacienteId, int codObraSociales) {
        this.dni = dni;
        this.nombre = nombre;
        this.email = email;
        this.habilitado = habilitado;
        this.fechaNacimiento = fechaNacimiento;
        this.sexo = sexo;
        this.pacienteId = pacienteId;
        this.codObraSociales = codObraSociales;
    }

    public Boolean updatePaciente(DtoPaciente pacientes) {

        for (int i = 0; i < _listaPacientes.size(); i++) {
            if (_listaPacientes.get(i).getPacienteId() == pacientes.getPacienteId()) {
                _listaPacientes.get(i).setDni(pacientes.getDni());
                _listaPacientes.get(i).setHabilitado(pacientes.getHabilitado());
                _listaPacientes.get(i).setEmail(pacientes.getEmail());
                _listaPacientes.get(i).setNombre(pacientes.getNombre());
                _listaPacientes.get(i).setCodObraSociales(pacientes.getCodObraSociales());
                _listaPacientes.get(i).setFechaNacimiento(pacientes.getFechaNacimiento());
                _listaPacientes.get(i).setSexo(pacientes.getSexo());

                JsonDb.save(Pacientes.class, _listaPacientes);
                return true;
            }
        }
        return false;
    }

    @Override
    public int deletePaciente(long pacienteId) {

        for (int i = 0; i < _listaPacientes.size(); i++) {
            if (_listaPacientes.get(i).getPacienteId() == pacienteId) {

               List<DtoPeticion> peticionesByDni = peticiones.obtenerPeticionesUsuarios(_listaPacientes.get(i).getDni());

               if(peticionesByDni !=null){
                for (int j = 0; j < peticionesByDni.size(); j++) {
                    if(peticionesByDni.get(i).getEstado().getEstados().equals(Estados.Finalizado)){
                    return 500;
                    }
                }
               }

                _listaPacientes.remove(i);
                JsonDb.save(Pacientes.class, _listaPacientes);
                return 200;
            }
        }
        return 404;
    }

    @Override
    public DtoPaciente getPacienteDNI(int dni) {
        return null;
    }

    public List<ObraSociales> getObraSociales() {
        return _obrasSociales;
    }

    public Sexo[] getSexoString() {
        return Sexo.values();
    }

    @Override
    public Boolean save(DtoPaciente pacientes) {
        Boolean response = false;

        if (pacientes.getPacienteId() <= 0 || pacientes.getPacienteId() == 404) {
            pacientes.setPacienteId(IdGenerator.generate());
            _listaPacientes.add(new Pacientes(pacientes.getDni(), pacientes.getNombre(), pacientes.getEmail(), pacientes.getHabilitado(), pacientes.getFechaNacimiento(), pacientes.getSexo(), pacientes.getPacienteId(), pacientes.getCodObraSociales()));
            JsonDb.save(Pacientes.class, _listaPacientes);
            response = true;
        } else {
            pacientes.setPacienteId(pacientes.getPacienteId());
            response = updatePaciente(pacientes);
        }

        return response;
    }

    @Override
    public int getCodObraSociales(String nombreObraSocial) {
        int codObraSocial = 0;

        for (ObraSociales _obraSocial : _obrasSociales) {
            if (_obraSocial.getNombreObraSocial().equals(nombreObraSocial)) {
                codObraSocial = _obraSocial.getCodObraSocial();
            }
        }
        return codObraSocial;
    }

    @Override
    public String getNombreObraSociales(int codObraSocial) {
        String nombreObraSocial = null;

        for (ObraSociales _obraSocial : _obrasSociales) {
            if (_obraSocial.getCodObraSocial() == codObraSocial) {
                nombreObraSocial = _obraSocial.getNombreObraSocial();
            }
        }
        return nombreObraSocial;
    }

    @Override
    public int calculateEdad(Date fechaNacimiento) {
        Date today = new Date();
        int edad = -1;

        if (today.getTime() > fechaNacimiento.getTime()) {
            long diffInMillies = Math.abs(today.getTime() - fechaNacimiento.getTime());
            edad = (int) (TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS)) / 365;
        }

        return edad;
    }

    @Override
    public DtoPaciente obtenerPaciente(long pacienteId) {
        DtoPaciente object;
        for (Pacientes listaPaciente : _listaPacientes) {
            if (listaPaciente.getPacienteId() == pacienteId) {
                object = new DtoPaciente();
                object.setPacienteId(listaPaciente.getPacienteId());
                object.setNombre(listaPaciente.getNombre());
                object.setSexo(listaPaciente.getSexo());
                object.setHabilitado(listaPaciente.getHabilitado());
                object.setFechaNacimiento(listaPaciente.getFechaNacimiento());
                object.setEmail(listaPaciente.getEmail());
                object.setDni(listaPaciente.getDni());
                object.setCodObraSociales(listaPaciente.getCodObraSociales());
                return object;
            }
        }
        return null;
    }

    @Override
    public DtoPaciente obtenerPacienteXDni(int dni) {
        DtoPaciente object;
        for (Pacientes listaPaciente : _listaPacientes) {
            if (listaPaciente.getDni() == dni) {
                object = new DtoPaciente();
                object.setPacienteId(listaPaciente.getPacienteId());
                object.setNombre(listaPaciente.getNombre());
                object.setSexo(listaPaciente.getSexo());
                object.setHabilitado(listaPaciente.getHabilitado());
                object.setFechaNacimiento(listaPaciente.getFechaNacimiento());
                object.setEmail(listaPaciente.getEmail());
                object.setDni(listaPaciente.getDni());
                object.setCodObraSociales(listaPaciente.getCodObraSociales());
                return object;
            }
        }
        return null;
    }
}
