package view.practicas;

import controller.ControllerPracticas;
import dto.DtoPracticas;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;


public class PracticasView {
    public JPanel pnlPracticas;
    private JButton btnCrearPractica;
    private JTable tblPracticas;
    private JPanel pnlListadoPracticas;
    DefaultTableModel tblPracticasModel;
    CrearPracticaView dialogCrearPractica;
    ControllerPracticas controllerPracticas = ControllerPracticas.getInstance();

    public PracticasView() {
        initPracticas();
        setupButtons();
        loadPractias();
    }

    private void initPracticas(){
        tblPracticasModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        tblPracticasModel.addColumn("Codigo");
        tblPracticasModel.addColumn("Nombre");
        tblPracticasModel.addColumn("Grupo");
        tblPracticasModel.addColumn("Tiempo");
        tblPracticasModel.addColumn("Habilitado");

        tblPracticas = new JTable(tblPracticasModel);

        tblPracticas.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                if (me.getClickCount() == 2) {
                    int row = tblPracticas.getSelectedRow();
                    String idSucursal = (String)tblPracticas.getModel().getValueAt(row, 0);
                    showEditPracticas(idSucursal);
                }
            }
        });

        pnlListadoPracticas.setLayout(new BorderLayout());
        pnlListadoPracticas.add(new JScrollPane(tblPracticas), BorderLayout.CENTER);
    }

    private void setupButtons(){
        btnCrearPractica.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showCrearPracticas();
            }
        });

    }

    void showCrearPracticas(){
        dialogCrearPractica = new CrearPracticaView(this);
        dialogCrearPractica.setVisible(true);
    }

    void showEditPracticas(String id){
        dialogCrearPractica = new CrearPracticaView(this);
        dialogCrearPractica.setEditing(id);
        dialogCrearPractica.setVisible(true);
    }

    public void dialogDisposed(){
        loadPractias();
    }

    public void loadPractias() {
        List<DtoPracticas> pract = controllerPracticas.getTodasPracticas(); // cuando este la practica
        tblPracticasModel.setRowCount(0);
        for (DtoPracticas dto : pract) {
            tblPracticasModel.addRow(new Object[]{
                    dto.getCodPractica(),
                    dto.getNombrePractica(),
                    dto.getGrupo(),
                    dto.getCantHorasResultado(),
                    dto.getHabilitado()
            });
        }
    }
}
