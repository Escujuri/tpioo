package model;

public class ObraSociales {

    private int codObraSocial;

    private String nombreObraSocial;

    // -- GETTER AND SETTER--//

    public int getCodObraSocial() {
        return codObraSocial;
    }

    public void setCodObraSocial(int codObraSocial) {
        this.codObraSocial = codObraSocial;
    }

    public String getNombreObraSocial() {
        return nombreObraSocial;
    }

    public void setNombreObraSocial(String nombreObraSocial) {
        this.nombreObraSocial = nombreObraSocial;
    }

    public String toString() {
        return String.format(this.nombreObraSocial);
    }

}
