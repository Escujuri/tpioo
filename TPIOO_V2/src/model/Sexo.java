package model;

public enum Sexo {
    Masculino("Masculino"),
    Femenino("Femenino"),
    X("X");

    private String descripcion;

    Sexo(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    @Override
    public String toString() {
        return this.descripcion;
    }
}