package model;

public enum NombresObrasSociales {
    OSDE("OSDE"),
    SW("Swiss Medical"),
    Omint("Omint"),
    Medicus("Medicus"),
    Galeno("Galeno");

    private String descripcion;

    NombresObrasSociales(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    @Override
    public String toString() {
        return this.descripcion;
    }
}