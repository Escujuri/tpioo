package dto;

import model.ObraSociales;

public class DtoObraSocial {

    private ObraSociales obraSociales;

    public ObraSociales getObraSociales() {
        return obraSociales;
    }

    public void setObraSociales(ObraSociales obraSociales) {
        this.obraSociales = obraSociales;
    }

}
