package dto;

import java.util.List;

public class DtoAnalisis {

    private int idAnalisis;

    private String nombreAnalisis;

    private int valorNormal;

    private int valorCritico;

    private int valorReservado;

    private String resultado;

    private List<DtoAnalisis> analisisList;

    public int getIdAnalisis() {
        return idAnalisis;
    }

    public void setIdAnalisis(int idAnalisis) {
        this.idAnalisis = idAnalisis;
    }

    public int getValorNormal() {
        return valorNormal;
    }

    public void setValorNormal(int valorNormal) {
        this.valorNormal = valorNormal;
    }

    public int getValorCritico() {
        return valorCritico;
    }

    public void setValorCritico(int valorCritico) {
        this.valorCritico = valorCritico;
    }

    public int getValorReservado() {
        return valorReservado;
    }

    public void setValorReservado(int valorReservado) {
        this.valorReservado = valorReservado;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public List<DtoAnalisis> getAnalisisList() {
        return analisisList;
    }

    public void setAnalisisList(List<DtoAnalisis> analisisList) {
        this.analisisList = analisisList;
    }

    public String toString() {
        return String.format(this.nombreAnalisis);
    }

    public String getNombreAnalisis() {
        return nombreAnalisis;
    }

    public void setNombreAnalisis(String nombreAnalisis) {
        this.nombreAnalisis = nombreAnalisis;
    }

}
