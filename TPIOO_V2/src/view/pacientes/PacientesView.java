package view.pacientes;

import controller.ControllerPacientes;
import dto.DtoPaciente;
import model.Pacientes;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

public class PacientesView {
    public  JPanel pnlPacientes;
    private JButton btnCrearPaciente;
    private JButton btnActualizarLista;
    private JPanel pnlListadoPacientes;
    private JPanel botones;
    private JTable tblPacientes;
    private JButton btnBuscarPaciente;
    private DefaultTableModel tblPacientesModel;
    CrearPacientesView dialogForm;
    ControllerPacientes controllerPacientes = ControllerPacientes.getInstance();

    public PacientesView(){
         Botones();
         initializeTablePacientes();
         loadPacientes();
    }

    private void initializeTablePacientes() {
        tblPacientesModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        tblPacientesModel.addColumn("Id");
        tblPacientesModel.addColumn("DNI");
        tblPacientesModel.addColumn("Nombre y Apellido");
        tblPacientesModel.addColumn("Mail");
        tblPacientesModel.addColumn("Sexo");
        tblPacientesModel.addColumn("Obra Social");
        tblPacientesModel.addColumn("Edad");
        tblPacientesModel.addColumn("Habilitado");

        tblPacientes = new JTable(tblPacientesModel);
       // tblPacientes.removeColumn(tblPacientes.getColumnModel().getColumn(0));

        tblPacientes.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                if (me.getClickCount() == 2) {
                    int row = tblPacientes.getSelectedRow();
                    long pacienteId = (long)tblPacientes.getModel().getValueAt(row, 0);

                    CrearPacientesView form = new CrearPacientesView(pacienteId);
                    form.setVisible(true);

                    loadPacientes();
                }
            }
        });

        pnlListadoPacientes.setLayout(new BorderLayout());
        pnlListadoPacientes.add(new JScrollPane(tblPacientes), BorderLayout.CENTER);

    }

    private void Botones() {
        // Menu Pacientes
        btnCrearPaciente.addActionListener(e -> {
            showCrearPacientes();
        });
        btnActualizarLista.addActionListener(e -> {
            loadPacientes();
        });
    }

    private void loadPacientes() {
        List<DtoPaciente> dtos = controllerPacientes.obtenerPacientes();

        System.out.println("loadPacientes");

        tblPacientesModel.setRowCount(0);

        for (DtoPaciente dto: dtos) {
            DtoPaciente paciente = dto;

            System.out.println("paso por aca 2");
            System.out.println(paciente.getCodObraSociales());

            tblPacientesModel.addRow(new Object[] {
                    paciente.getPacienteId(),
                    paciente.getDni(),
                    paciente.getNombre(),
                    paciente.getEmail(),
                    paciente.getSexo(),
                    controllerPacientes.getNombreObraSociales(paciente.getCodObraSociales()),
                    controllerPacientes.calculateEdad(paciente.getFechaNacimiento()),
                    paciente.getHabilitado()
            });
        }
    }

    void showCrearPacientes(){
        dialogForm = new CrearPacientesView(404);
        dialogForm.setVisible(true);
    }
}
