package view;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginView  {
    private JPanel panel;
    private JTextField txt_password;
    private JTextField txt_username;
    private JButton loginButton;
    private JFrame rootView = new JFrame("Login");


    public LoginView() {
        rootView.setContentPane(this.panel);
        rootView.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        rootView.setSize(400, 400);
        rootView.setLocationRelativeTo(null);

        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                goToHome();
            }
        });
    }

    public void mostrar() {
        rootView.setVisible(true);
    }


    public void goToHome(){
        /*
        HomeView homeView = new HomeView();
        homeView.mostrar();
        close();

         */
    }

    public void close(){
        rootView.setVisible(false);
        rootView.dispose();
    }
}
