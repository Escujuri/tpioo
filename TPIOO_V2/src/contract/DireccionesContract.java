package contract;

import dto.DtoDireccion;

public interface DireccionesContract {

    Boolean crearDireccionUsuario(DtoDireccion altaDireccionUsuario);

    Boolean modificarDireccionUsuario(DtoDireccion modificarDireccionUsuario);

    Boolean borrarDireccionUsuario(int idDireccionUsuario);

    DtoDireccion obtenerDireccionUsuario(int idDireccionUsuario);

    Boolean crearDireccionPaciente(DtoDireccion altaDireccionPaciente);

    Boolean modificarDireccionPaciente(DtoDireccion modificarDireccionPaciente);

    Boolean borrarDireccionPaciente(int idDireccionPaciente);

    DtoDireccion obtenerDireccionPaciente(int idDireccionPaciente);

    Boolean crearDireccionSucursal(DtoDireccion altaDireccionSucursal);

    Boolean modificarDireccionSucursal(DtoDireccion modificarDireccionSucursal);

    Boolean borrarDireccionSucursal(int idDireccionSucursal);

    DtoDireccion obtenerDireccionSucursal(int idDireccionSucursal);
}
