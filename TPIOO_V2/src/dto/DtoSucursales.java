package dto;

import model.Sucursales;

public class DtoSucursales {

    private int numeroSucursal;

    private Boolean habilitado;

    private String nombreSucursal;

    private String responsableTecnico;

    private DtoDireccion direccion;

    // -- GETTER AND SETTER--//
    public int getNumeroSucursal() {
        return numeroSucursal;
    }

    public void setNumeroSucursal(int numeroSucursal) {
        this.numeroSucursal = numeroSucursal;
    }

    public Boolean getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(Boolean habilitado) {
        this.habilitado = habilitado;
    }

    public String getNombreSucursal() {
        return nombreSucursal;
    }

    public void setNombreSucursal(String nombreSucursal) {
        this.nombreSucursal = nombreSucursal;
    }

    public DtoDireccion getDireccion() {
        return direccion;
    }

    public void setDireccion(DtoDireccion direccion) {
        this.direccion = direccion;
    }

    public String getResponsableTecnico() {
        return responsableTecnico;
    }

    public void setResponsableTecnico(String responsableTecnico) {
        this.responsableTecnico = responsableTecnico;
    }

    public String toString() {
        return String.format(this.nombreSucursal);
    }
}
