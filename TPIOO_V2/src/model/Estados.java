package model;

public enum Estados {
    Pendiente("Pendiente"),
    Parcial("Parcial"),
    Finalizado("Finalizado");

    private String estados;

    Estados(String estados) {
        this.estados = estados;
    }

    public String getEstados() {
        return estados;
    }

    @Override
    public String toString() {
        return this.estados;
    }
}