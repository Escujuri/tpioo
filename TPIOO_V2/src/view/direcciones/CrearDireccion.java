package view.direcciones;

import dto.DtoDireccion;
import view.BaseDialog;


import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class CrearDireccion extends JDialog {
    private JTextField txtCalle;
    private JTextField txtAltura;
    private JTextField txtCiudad;
    private JTextField txtProvincia;
    private JTextField txtPais;
    private JTextField txtTelefono;
    private JButton cancelButton;
    private JButton okButton;
    private JLabel jLabel;
    private JPanel pnlDireccion;
    private BaseDialog frame;

    public CrearDireccion(BaseDialog parentFrame){
        frame = parentFrame;
        initView();
    }

    void initView() {
        setTitle("Añadir direccion");
        setModal(true);
        setContentPane(pnlDireccion);
        setSize(400, 450);
        setLocationRelativeTo(null);
        setAlwaysOnTop(true);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
        setupButtons();
    }

    private void setupButtons(){
        okButton.addActionListener(e -> okPressed());
        cancelButton.addActionListener(e -> onCancel());
    }

    private Boolean validateInputUI () {
        Boolean response = false;

        if (txtCalle.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Ingrese una calle");
        }
        else if (txtAltura.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Ingrese una altura");
        }
        else if (txtCiudad.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Ingrese una ciudad");
        }
        else if (txtProvincia.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Ingrese una provincia");
        }
        else if (txtPais.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Ingrese un pais");
        }
        else if (txtTelefono.getText().length() != 10) {
            JOptionPane.showMessageDialog(this, "El numero de telefono debe tener 10 digitos");
        } else {
            response = true;
        }

        return response;
    }

    private void okPressed(){

        try {
            if (validateInputUI()) {
                DtoDireccion direccion = new DtoDireccion();

                direccion.setAltura(Integer.parseInt(txtAltura.getText()));
                direccion.setProvincia(txtProvincia.getText());
                direccion.setPais(txtPais.getText());
                direccion.setCalle(txtCalle.getText());
                direccion.setCiudad(txtCiudad.getText());
                direccion.setTelefono(Integer.parseInt(txtTelefono.getText()));

                frame.getDireccionInfo(direccion);
                dispose();
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

    private void onCancel(){
        dispose();
    }

}
