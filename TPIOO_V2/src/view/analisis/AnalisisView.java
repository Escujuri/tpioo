package view.analisis;


import controller.ControllerAnalisis;
import dto.DtoAnalisis;
import view.BaseJframe;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

public class AnalisisView extends BaseJframe {
    public JPanel pnlAnalisis;
    private JButton crearAnalisisButton;
    private JTable tblAnalisis;
    private JPanel pnlAnalisisLista;
    CrearAnalisiView dialogForm;
    private DefaultTableModel tblAnalisisModel;
    ControllerAnalisis controllerAnalisis =  ControllerAnalisis.getInstance();

    public AnalisisView(){
        initSucursales();
        setupButtons();
        loadAnalisis();
    }

    private void initSucursales(){
        dialogForm = new CrearAnalisiView(this);

        tblAnalisisModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        tblAnalisisModel.addColumn("Id");
        tblAnalisisModel.addColumn("Nombre");
        tblAnalisisModel.addColumn("Valor normal");
        tblAnalisisModel.addColumn("Valor reservado");
        tblAnalisisModel.addColumn("Valor critico");

        tblAnalisis = new JTable(tblAnalisisModel);

        tblAnalisis.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                if (me.getClickCount() == 2) {
                    int row = tblAnalisis.getSelectedRow();
                    int idSucursal = (int)tblAnalisis.getModel().getValueAt(row, 0);
                    showEditarAnalisis(idSucursal);
                }
            }
        });

        pnlAnalisisLista.setLayout(new BorderLayout());
        pnlAnalisisLista.add(new JScrollPane(tblAnalisis), BorderLayout.CENTER);
    }

    public void dialogDisposed(){
        loadAnalisis();
    }

    private void loadAnalisis(){

       List<DtoAnalisis> analisisLi = controllerAnalisis.getTodosAnalisis();
        tblAnalisisModel.setRowCount(0);

        for (DtoAnalisis dto: analisisLi) {

            tblAnalisisModel.addRow(new Object[] {
                    dto.getIdAnalisis(),
                    dto.getNombreAnalisis(),
                    dto.getValorNormal(),
                    dto.getValorReservado(),
                    dto.getValorCritico()
            });
        }
    }

    private void setupButtons(){
        crearAnalisisButton.addActionListener(e-> showCrearAnalisis());
    }

    private void  showCrearAnalisis() {
        dialogForm = new CrearAnalisiView(this);
        dialogForm.setVisible(true);
    }


    private void  showEditarAnalisis(int id){
        dialogForm = new CrearAnalisiView(this);
        dialogForm.setEditing(id);
        dialogForm.setVisible(true);
    }



}
