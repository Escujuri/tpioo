package contract;

import dto.DtoSucursales;

import java.util.List;

public interface SucursalContract {

    List<DtoSucursales> allSucursales();

    DtoSucursales obtenerSucursal(int id);

    Boolean crearSucursal(DtoSucursales sucursal);

    Boolean borrarSucursal(int numeroSucursal);

    Boolean actualizarSucursal(DtoSucursales sucursal);

    Boolean existeSucursal(int numeroSucursal);

    int obtenerIdSucursalByNombre(String nombreSucursal);
}