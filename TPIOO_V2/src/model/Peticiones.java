package model;

import contract.PeticionesContract;
import dto.DtoEstadosPeticiones;
import dto.DtoPeticion;
import dto.DtoPracticas;
import dto.DtoSucursales;
import utils.JsonDb;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class Peticiones implements PeticionesContract {

    private int idPeticion;

    private long idPaciente;

    private EstadosPeticiones estado;

    private Boolean tieneObraSocial;

    private Date fechaCarga;

    private String nombreSucursal;

    private Practicas practicasService = new Practicas();

    private ResultadoPeticion resultadoPeticion = new ResultadoPeticion();

    private ObraSociales obraSociales = new ObraSociales();

    private Date fechaCalculadaEntrega;

    private List<DtoPracticas> practicas;

    private static List<Peticiones> listaPeticiones;


    static {
        listaPeticiones = utils.JsonDb.getAll(Peticiones.class);
    }

    public Peticiones() {
    }

    public Peticiones(int idPeticion, long idPaciente, EstadosPeticiones estado, Boolean tieneObraSocial,
                      Date fechaCarga, ObraSociales obraSociales, Date fechaCalculadaEntrega, List<DtoPracticas> practicas,String nombreSucursal) {

        this.idPeticion = idPeticion;
        this.idPaciente = idPaciente;
        this.estado = estado;
        this.tieneObraSocial = tieneObraSocial;
        this.fechaCarga = fechaCarga;
        this.obraSociales = obraSociales;
        this.fechaCalculadaEntrega = fechaCalculadaEntrega;
        this.practicas = practicas;
        this.nombreSucursal = nombreSucursal;

    }

    // -- GETTER AND SETTER--//

    public int getIdPeticion() {
        return idPeticion;
    }

    public String getNombreSucursal() {
        return nombreSucursal;
    }

    public void setIdPeticion(int idPeticion) {
        this.idPeticion = idPeticion;
    }

    public Boolean getTieneObraSocial() {
        return tieneObraSocial;
    }

    public void setTieneObraSocial(Boolean tieneObraSocial) {
        this.tieneObraSocial = tieneObraSocial;
    }

    public Date getFechaCarga() {
        return fechaCarga;
    }

    public void setFechaCarga(Date fechaCarga) {
        this.fechaCarga = fechaCarga;
    }

    public Date getFechaCalculadaEntrega() {
        return fechaCalculadaEntrega;
    }

    public void setFechaCalculadaEntrega(Date fechaCalculadaEntrega) {
        this.fechaCalculadaEntrega = fechaCalculadaEntrega;
    }

    public long getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(long idPaciente) {
        this.idPaciente = idPaciente;
    }

    public List<DtoPracticas> getPracticas() {
        return practicas;
    }

    public void setPracticas(List<DtoPracticas> practicas) {
        this.practicas = practicas;
    }

    public EstadosPeticiones getEstado() {
        return estado;
    }

    public void setEstado(EstadosPeticiones estado) {
        this.estado = estado;
    }

    public void setNombreSucursal(String nombreSucursal) {
        this.nombreSucursal = nombreSucursal;
    }

    @Override
    public Boolean crearPeticion(DtoPeticion peticion) {
        if (this.existePeticion(peticion.getIdPeticion())) {
            return false;
        } else {

            EstadosPeticiones estadoInicial = new EstadosPeticiones();
            estadoInicial.setNombreSucursal(peticion.getNombreSucursal());
            estadoInicial.setEstados(Estados.Pendiente);
            estadoInicial.setNombreSucursal(peticion.getNombreSucursal());

            ObraSociales obraSocial = null;

            if(peticion.getTieneObraSocial()){
                obraSocial = new ObraSociales();
                obraSocial.setCodObraSocial(peticion.getSetCodObraSocial());
                obraSocial.setNombreObraSocial(peticion.getNombreObraSocial());
            }

            listaPeticiones.add(new Peticiones(
                    peticion.getIdPeticion(),
                    peticion.getIdPaciente(),
                    estadoInicial,
                    peticion.getTieneObraSocial(),
                    peticion.getFechaCarga(),
                    obraSociales,
                    peticion.getFechaCalculadaEntrega(),
                    peticion.getPracticas(),
                    peticion.getNombreSucursal()
            ));

            JsonDb.save(Peticiones.class, listaPeticiones);
            return true;
        }
    }

    @Override
    public Boolean existePeticion(int idPeticion) {
        for (Peticiones listaPeticiones : listaPeticiones) {
            if (listaPeticiones.getIdPeticion() == idPeticion) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Boolean eliminarPeticion(int idPeticion) {
        for (int i = 0; i < listaPeticiones.size(); i++) {
            if (listaPeticiones.get(i).getIdPeticion() == idPeticion) {
                listaPeticiones.remove(i);
                JsonDb.save(Peticiones.class, listaPeticiones);
                return true;
            }
        }
        return false;
    }

    @Override
    public Boolean actualizarPeticion(DtoPeticion peticion) {
        for (int i = 0; i < listaPeticiones.size(); i++) {

            if (listaPeticiones.get(i).getIdPeticion() == peticion.getIdPeticion()) {
                if (listaPeticiones.get(i).getFechaCalculadaEntrega() != null)
                    listaPeticiones.get(i).setFechaCalculadaEntrega(peticion.getFechaCalculadaEntrega());
                if (peticion.getFechaCarga() != null)
                    listaPeticiones.get(i).setFechaCarga(peticion.getFechaCarga());
                if (peticion.getTieneObraSocial() != null)
                    listaPeticiones.get(i).setTieneObraSocial(peticion.getTieneObraSocial());
                if (peticion.getEstado() != null)
                    listaPeticiones.get(i).setTieneObraSocial(peticion.getTieneObraSocial());
                if (peticion.getIdPaciente() != 0)
                    listaPeticiones.get(i).setIdPaciente(peticion.getIdPaciente());
                if (peticion.getNombreSucursal() != null)
                    listaPeticiones.get(i).setNombreSucursal(peticion.getNombreSucursal());

                JsonDb.save(Peticiones.class, listaPeticiones);
                return true;
            }
        }
        return false;
    }

    @Override
    public Boolean actualizarEstadoPeticion(DtoEstadosPeticiones estadosPeticiones) {
        for (int i = 0; i < listaPeticiones.size(); i++) {
            if (listaPeticiones.get(i).getIdPeticion() == estadosPeticiones.getIdPeticion()) {
                if (estadosPeticiones.getEstados() != null)
                    listaPeticiones.get(i).getEstado().setEstados(estadosPeticiones.getEstados());
                if (estadosPeticiones.getDescripcion() != null)
                    listaPeticiones.get(i).getEstado().setDescripcion(estadosPeticiones.getDescripcion());
                if (estadosPeticiones.getNombreSucursal() != null)
                    listaPeticiones.get(i).getEstado().setNombreSucursal(estadosPeticiones.getNombreSucursal());

                JsonDb.save(Peticiones.class, listaPeticiones);
                return true;
            }
        }
        return false;
    }

    @Override
    public List<DtoPeticion> obtenerPeticionesUsuarios(int dni) {
        DtoPeticion response = null;
        List<DtoPeticion> listaPeticionesUsuarios = new ArrayList<DtoPeticion>();

        for (Peticiones peticiones : listaPeticiones) {
            if (peticiones.getIdPaciente() == dni) {
                response = new DtoPeticion();
                response.setPacienteId(peticiones.getIdPaciente());
                response.setIdPeticion(peticiones.getIdPeticion());
                response.setFechaCarga(peticiones.getFechaCarga());
                response.setFechaCalculadaEntrega(peticiones.getFechaCalculadaEntrega());
                response.setTieneObraSocial(peticiones.getTieneObraSocial());
                response.setEstado(peticiones.estado);

                listaPeticionesUsuarios.add(response);
            }
        }
        return listaPeticionesUsuarios;
    }

    @Override
    public List<DtoPeticion> obtenerPeticionesSucursales(String nombreSucursal) {
        DtoPeticion response = null;
        List<DtoPeticion> listaPeticionesUsuarios = new ArrayList<DtoPeticion>();

        for (Peticiones peticiones : listaPeticiones) {
            if (Objects.equals(peticiones.getNombreSucursal(), nombreSucursal)) {
                response = new DtoPeticion();
                response.setPacienteId(peticiones.getIdPaciente());
                response.setIdPeticion(peticiones.getIdPeticion());
                response.setFechaCarga(peticiones.getFechaCarga());
                response.setFechaCalculadaEntrega(peticiones.getFechaCalculadaEntrega());
                response.setTieneObraSocial(peticiones.getTieneObraSocial());
                response.setEstado(peticiones.estado);

                listaPeticionesUsuarios.add(response);
            }
        }
        return listaPeticionesUsuarios;
    }

    @Override
    public List<DtoPeticion> obtenerPeticiones() {
        DtoPeticion object = null;
        List<DtoPeticion> listPeticionResp = new ArrayList<DtoPeticion>();

        for (int i = 0; i < listaPeticiones.size(); i++) {
            object = new DtoPeticion();
            object.setIdPeticion(listaPeticiones.get(i).getIdPeticion());
            object.setFechaCarga(listaPeticiones.get(i).getFechaCarga());
            object.setPracticas(listaPeticiones.get(i).getPracticas());
            object.setEstado(listaPeticiones.get(i).getEstado());
            object.setTieneObraSocial(listaPeticiones.get(i).getTieneObraSocial());
            object.setPacienteId(listaPeticiones.get(i).getIdPaciente());
            object.setFechaCalculadaEntrega(listaPeticiones.get(i).getFechaCalculadaEntrega());
            object.setNombreSucursal(listaPeticiones.get(i).getNombreSucursal());

            listPeticionResp.add(object);
        }
        return listPeticionResp;

    }

    @Override
    public DtoPeticion obtenerPeticion(int id) {
        DtoPeticion object;
        for (int i = 0; i < listaPeticiones.size(); i++) {
            if (listaPeticiones.get(i).getIdPeticion() == id) {
                object = new DtoPeticion();
                object.setIdPeticion(listaPeticiones.get(i).getIdPeticion());
                object.setFechaCarga(listaPeticiones.get(i).getFechaCarga());
                object.setPracticas(listaPeticiones.get(i).getPracticas());
                //object.setNombreSucursal(listaPeticiones.get(i).getEstado());
                object.setTieneObraSocial(listaPeticiones.get(i).getTieneObraSocial());
                object.setPacienteId(listaPeticiones.get(i).getIdPaciente());
                object.setFechaCalculadaEntrega(listaPeticiones.get(i).getFechaCalculadaEntrega());
                object.setNombreSucursal(listaPeticiones.get(i).getNombreSucursal());
                return object;
            }
        }
        return null;
    }

}
