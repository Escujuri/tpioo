package dto;

import model.Pacientes;

import java.util.Date;

public class DtoPaciente {

	private Pacientes paciente;

	private long pacienteId;

	private int dni;

	private String nombre;

	private String email;

	private int codObraSociales;

	private String sexo;

	private Date fechaNacimiento;

	private Boolean habilitado;

	// -- GETTER AND SETTER--//

	public int getDni() {
		return dni;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public Boolean getHabilitado() {
		return habilitado;
	}

	public void setHabilitado(Boolean habilitado) {
		this.habilitado = habilitado;
	}

	public Pacientes getPacientes() { return paciente; }

	public void setPacientes(Pacientes pacientes) { this.paciente = pacientes; }

	public long getPacienteId() {
		return pacienteId;
	}

	public void setPacienteId(long pacienteId) {
		this.pacienteId = pacienteId;
	}

	public int getCodObraSociales() {
		return codObraSociales;
	}

	public void setCodObraSociales(int codObraSociales) {
		this.codObraSociales = codObraSociales;
	}

	public String toString() {
		return String.format(String.valueOf(this.dni));
	}

}
