package model;

import contract.UsuarioContract;
import dto.DtoDireccion;
import dto.DtoUsuarios;
import utils.JsonDb;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Usuarios implements UsuarioContract {

    private int dni;

    private String nombreUsuario;

    private String nombre;

    private String email;

    private String contrasenia;

    private Date fechaNacimiento;

    private Boolean habilitado;

    private Roles rolUsuario;

    private static List<Usuarios> listaUsuarios;

    private Direcciones direccionService = new Direcciones();

    static {
        listaUsuarios = utils.JsonDb.getAll(Usuarios.class);
    }

    // -- GETTER AND SETTER--//
    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public Boolean getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(Boolean habilitado) {
        this.habilitado = habilitado;
    }

    public Roles getRolUsuario() {
        return rolUsuario;
    }

    public void setRolUsuario(Roles rolUsuario) {
        this.rolUsuario = rolUsuario;
    }

    public Usuarios() {
    }

    @Override
    public Boolean crearUsuario(DtoUsuarios nuevoUsuario) {
        if (this.existeUsuario(nuevoUsuario.getDni())) {
            return false;
        } else {
            this.listaUsuarios.add(new Usuarios(nuevoUsuario));
            nuevoUsuario.getDireccion().setIdDireccionUsuario(nuevoUsuario.getDni());
            direccionService.crearDireccionUsuario(nuevoUsuario.getDireccion());
            JsonDb.save(Usuarios.class, listaUsuarios);
            return true;
        }
    }

    @Override
    public Boolean asignarRol(int dni, String nuevoRol) {
        for (int i = 0; i < listaUsuarios.size(); i++) {
            if (listaUsuarios.get(i).getDni() == dni) {
                Roles rol = new Roles();
                rol.setNombreRol(nuevoRol);
                this.listaUsuarios.get(i).setRolUsuario(rol);
                JsonDb.save(Usuarios.class, listaUsuarios);
                return true;
            }
        }
        return false;
    }

    @Override
    public Boolean modificarUsuario(DtoUsuarios modificarUsuario) {
        for (int i = 0; i < listaUsuarios.size(); i++) {
            if (listaUsuarios.get(i).getDni() == modificarUsuario.getDni()) {
                if (modificarUsuario.getNombreUsuario() != null)
                    listaUsuarios.get(i).setNombreUsuario(modificarUsuario.getNombreUsuario());
                if (modificarUsuario.getContrasenia() != null)
                    listaUsuarios.get(i).setContrasenia(modificarUsuario.getContrasenia());
                if (modificarUsuario.getNombre() != null)
                    listaUsuarios.get(i).setNombre(modificarUsuario.getNombre());
                if (modificarUsuario.getEmail() != null)
                    listaUsuarios.get(i).setEmail(modificarUsuario.getEmail());
                if (modificarUsuario.getNombreUsuario() != null)
                    listaUsuarios.get(i).setNombreUsuario(modificarUsuario.getNombreUsuario());
                if (modificarUsuario.getFechaNacimiento() != null)
                    listaUsuarios.get(i).setFechaNacimiento(modificarUsuario.getFechaNacimiento());

                direccionService.crearDireccionUsuario(modificarUsuario.getDireccion());
                JsonDb.save(Usuarios.class, listaUsuarios);
                return true;
            }
        }
        return false;
    }

    @Override
    public Boolean borrarUsuario(int dni) {
        for (int i = 0; i < listaUsuarios.size(); i++) {
            if (listaUsuarios.get(i).getDni() == dni) {
                this.listaUsuarios.remove(i);
                JsonDb.save(Usuarios.class, listaUsuarios);
                return true;
            }
        }
        return false;
    }

    @Override
    public List<DtoUsuarios> allUsuarios() {
        DtoUsuarios object = null;

        List<DtoUsuarios> listaUsuariosResponse = new ArrayList<DtoUsuarios>();

        for (Usuarios listaUsuario : listaUsuarios) {
            object = new DtoUsuarios();

            object.setDni(listaUsuario.getDni());
            object.setNombreUsuario(listaUsuario.getNombreUsuario());
            object.setNombre(listaUsuario.getNombre());
            object.setEmail(listaUsuario.getEmail());
            object.setContrasenia(listaUsuario.getContrasenia());
            object.setFechaNacimiento(listaUsuario.getFechaNacimiento());
            object.setHabilitado(listaUsuario.getHabilitado());

            DtoDireccion direccion = direccionService.obtenerDireccionUsuario(listaUsuario.getDni());
            object.setDireccion(direccion);
            listaUsuariosResponse.add(object);
        }
        return listaUsuariosResponse;
    }

    @Override
    public DtoUsuarios obtenerUsuario(int id) {
        DtoUsuarios object = null;
        for (Usuarios listaUsuario : listaUsuarios) {
            if(listaUsuario.getDni() == id){
                object = new DtoUsuarios();
                object.setDni(listaUsuario.getDni());
                object.setNombreUsuario(listaUsuario.getNombreUsuario());
                object.setNombre(listaUsuario.getNombre());
                object.setEmail(listaUsuario.getEmail());
                object.setContrasenia(listaUsuario.getContrasenia());
                object.setFechaNacimiento(listaUsuario.getFechaNacimiento());
                object.setHabilitado(listaUsuario.getHabilitado());
                DtoDireccion direccion = direccionService.obtenerDireccionUsuario(listaUsuario.getDni());
                object.setDireccion(direccion);
            }
        }
        return object;
    }

    @Override
    public Boolean existeUsuario(int dni) {
        for (Usuarios listaUsuario : listaUsuarios) {
            if (listaUsuario.getDni() == dni) {
                return true;
            }
        }
        return false;
    }

    private Usuarios(DtoUsuarios nuevoUsuario) {
        this.dni = nuevoUsuario.getDni();
        this.nombreUsuario = nuevoUsuario.getNombreUsuario();
        this.nombre = nuevoUsuario.getNombre();
        this.email = nuevoUsuario.getEmail();
        this.contrasenia = nuevoUsuario.getContrasenia();
        this.fechaNacimiento = nuevoUsuario.getFechaNacimiento();
        this.habilitado = nuevoUsuario.getHabilitado();
    }
}
