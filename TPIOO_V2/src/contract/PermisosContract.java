package contract;

import dto.DtoPermisos;

public interface PermisosContract {

    Boolean crearPermiso(DtoPermisos permiso);

    Boolean modificarPermiso(DtoPermisos permiso);

    Boolean borrarPermiso(String permiso);

}
