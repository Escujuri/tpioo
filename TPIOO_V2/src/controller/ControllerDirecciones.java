package controller;

import contract.DireccionesContract;
import dto.DtoDireccion;
import dto.DtoUsuarios;
import model.Direcciones;

import java.util.List;

public class ControllerDirecciones {

    private static ControllerDirecciones cu = null;

    private DireccionesContract sucursalContract = new Direcciones();

    private ControllerDirecciones() {
    }

    public static synchronized ControllerDirecciones getInstance() {
        if (cu == null) {
            cu = new ControllerDirecciones();
        }
        return cu;
    }


    public Boolean crearDireccionUsuario(DtoDireccion altaDireccionUsuario) {
        return sucursalContract.crearDireccionUsuario(altaDireccionUsuario);
    }

    public Boolean modificarDireccionUsuario(DtoDireccion modificarDireccionUsuario) {
        return sucursalContract.modificarDireccionUsuario(modificarDireccionUsuario);
    }

    public Boolean borrarDireccionUsuario(int idDireccionUsuario) {
        return sucursalContract.borrarDireccionUsuario(idDireccionUsuario);
    }

    public DtoDireccion obtenerDireccionUsuario(int idDireccionUsuario) {
        return sucursalContract.obtenerDireccionUsuario(idDireccionUsuario);
    }

    public DtoDireccion obtenerDireccionPaciente(int idDireccionPaciente){
        return sucursalContract.obtenerDireccionPaciente(idDireccionPaciente);
    }

    public Boolean crearDireccionPaciente(DtoDireccion altaDireccionPaciente) {
        return sucursalContract.crearDireccionPaciente(altaDireccionPaciente);
    }

    public Boolean modificarDireccionPaciente(DtoDireccion modificarDireccionPaciente) {
        return sucursalContract.modificarDireccionPaciente(modificarDireccionPaciente);
    }

    public Boolean borrarDireccionPaciente(int idDireccionPaciente) {
        return sucursalContract.borrarDireccionPaciente(idDireccionPaciente);
    }

    public DtoDireccion obtenerDireccionSucursal(int idDireccionSucursal){
        return sucursalContract.obtenerDireccionSucursal(idDireccionSucursal);
    }

    public Boolean crearDireccionSucursal(DtoDireccion altaDireccionSucursal) {
        return sucursalContract.crearDireccionSucursal(altaDireccionSucursal);
    }

    public Boolean modificarDireccionSucursal(DtoDireccion modificarDireccionSucursal) {
        return sucursalContract.modificarDireccionSucursal(modificarDireccionSucursal);
    }

    public Boolean borrarDireccionSucursal(int idDireccionSucursal) {
        return sucursalContract.borrarDireccionSucursal(idDireccionSucursal);
    }

}
