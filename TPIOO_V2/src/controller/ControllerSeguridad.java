package controller;

import contract.PermisosContract;
import contract.RolesContract;
import dto.DtoPermisos;
import dto.DtoRoles;
import model.Permisos;
import model.Roles;

import java.util.List;

public class ControllerSeguridad {

    private static ControllerSeguridad cu = null;

    private RolesContract rolesContract = new Roles();

    private PermisosContract PermisosContract = new Permisos();

    public static synchronized ControllerSeguridad getInstance() {
        if (cu == null) {
            cu = new ControllerSeguridad();
        }
        return cu;
    }

    private ControllerSeguridad() {
    }

    public boolean crearPermiso(DtoPermisos permiso) {
        return PermisosContract.crearPermiso(permiso);
    }

    public void modificarPermiso(DtoPermisos permiso) {
        PermisosContract.modificarPermiso(permiso);
    }

    public void borrarPermiso(String permiso) {
        PermisosContract.borrarPermiso(permiso);
    }

    public void crearRol(DtoRoles rol) {
        rolesContract.crearRol(rol);
    }

    public void modificarRol(DtoRoles rol) {
        rolesContract.modificarRol(rol);
    }

    public void borrarRol(String rol) {
        rolesContract.borrarRol(rol);
    }

    public List<DtoRoles> obtenerRoles(){return rolesContract.obtenerRoles();}

    public void setearRolActual(DtoRoles rol) {rolesContract.setearRolActual(rol);}

    public String obtenerRolActual() {return rolesContract.obtenerRolActual();}

}
