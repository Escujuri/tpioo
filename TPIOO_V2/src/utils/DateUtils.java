package utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
    private static DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    public static Date parse(String input) throws ParseException {
        return dateFormat.parse(input);
    }

    public static String toString(Date input) {
        return dateFormat.format(input);
    }
}