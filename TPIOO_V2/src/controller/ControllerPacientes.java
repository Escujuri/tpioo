package controller;


import contract.PacientesContract;
import dto.DtoPaciente;
import model.*;

import java.util.Date;
import java.util.List;

public class ControllerPacientes {

	private static ControllerPacientes cu = null;

	PacientesContract paciente = new Pacientes();

	private ControllerPacientes() {
	}

	public static synchronized ControllerPacientes getInstance() {
		if (cu == null) {
			cu = new ControllerPacientes();
		}
		return cu;
	}

	public Boolean save(DtoPaciente pacienteDTO) {
		return paciente.save(pacienteDTO);
	}

	public int calculateEdad(Date fechaNacimiento) {
		return paciente.calculateEdad(fechaNacimiento);
	}

	public int deletePaciente(long pacienteId) {
		return paciente.deletePaciente(pacienteId);
	}

	public int getCodObraSociales(String nombreObraSocial) {
		return paciente.getCodObraSociales(nombreObraSocial);
	}

	public String getNombreObraSociales(int codObraSocial) {
		return paciente.getNombreObraSociales(codObraSocial);
	}

	public List<DtoPaciente> obtenerPacientes() {
		return paciente.allPacientes();
	}

	public Sexo[] getSexoString() {
		return paciente.getSexoString();
	}

	public List<ObraSociales> getObraSociales() {
		return paciente.getObraSociales();
	}

	public DtoPaciente obtenerPaciente(long pacienteId) {
		return paciente.obtenerPaciente(pacienteId);
	}

	public DtoPaciente obtenerPacienteXDni(int dni) { return paciente.obtenerPacienteXDni(dni);}

}
