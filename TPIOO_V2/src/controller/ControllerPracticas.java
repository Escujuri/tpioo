package controller;

import contract.PracticasContract;
import dto.DtoAnalisis;
import dto.DtoPracticas;
import model.Analisis;
import model.Practicas;

import java.util.List;

public class ControllerPracticas {

    private static ControllerPracticas cp = null;

    private PracticasContract practica = new Practicas();

    public static synchronized ControllerPracticas getInstance() {
        if (cp == null) {
            cp = new ControllerPracticas();
        }
        return cp;
    }

    public Boolean crearPractica(DtoPracticas PracticasDto) {
        return practica.crearPractica(PracticasDto);
    }

    public Boolean actualizarPractica(DtoPracticas PracticasDto) {
        return practica.actualizarPractica(PracticasDto);
    }

    public Boolean inhabilitarPractica(String codPractica) {
        return practica.inhabilitarPractica(codPractica);
    }

    public DtoPracticas getPracticaById(String codPractica) {
        return practica.getPracticaById(codPractica);
    }

    public Boolean existeValoresCriticos(String codPractica) {
        return practica.existeValoresCriticos(codPractica);
    }

    public Boolean existeValoresReservados(String codPractica) {
        return practica.existeValoresReservados(codPractica);
    }

    public Analisis getAnalisisById(String idAnalisis) {
        return practica.getAnalisisById(idAnalisis);
    }

    public List<DtoAnalisis> getTodosAnalisis() {
        return practica.getTodosAnalisis();
    }

    public List<DtoPracticas> getTodasPracticas() { return practica.getTodasPracticas(); }
}