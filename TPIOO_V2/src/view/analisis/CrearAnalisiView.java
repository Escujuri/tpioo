package view.analisis;

import controller.ControllerAnalisis;
import dto.DtoAnalisis;
import view.BaseDialog;
import view.BaseJframe;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class CrearAnalisiView extends BaseDialog {

    private JTextField txtIdAnalisi;
    private JTextField txtNombre;
    private JTextField txtVnormal;
    private JTextField txtVcritico;
    private JTextField txtVReservado;
    private JButton cancelButton;
    private JButton okButton;
    private JButton borrarAnalisisButton;
    private JPanel pnlAnalisis;
    BaseJframe frame;
    ControllerAnalisis controllerAnalisis = ControllerAnalisis.getInstance();


    public CrearAnalisiView(BaseJframe parentFrame){
      frame = parentFrame;
      initView();
    }

    public CrearAnalisiView(){
        initView();
    }

    private void initView(){
        setTitle("Crear Analisis");
        setModal(true);
        setContentPane(pnlAnalisis);
        setSize(400, 450);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
        setupButtons();
    }


    private void  onCancel(){
        dispose();
    }

    private void setupButtons() {
        okButton.addActionListener(e-> okPressed());
        cancelButton.addActionListener(e-> onCancel());
    }

    private void borrarAnalisis(int id) {
        try {
            controllerAnalisis.borrarAnalisis(id);
            frame.dialogDisposed();
            JOptionPane.showMessageDialog(this, "El analisis: " + id + ", fue eliminado correctamente");
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, "Error2: " + ex.getMessage());
            return;
        }
        dispose();
    }

    public void setEditing(int id){
        borrarAnalisisButton.setEnabled(true);
        borrarAnalisisButton.addActionListener(e-> borrarAnalisis(id) );
        DtoAnalisis dto = controllerAnalisis.obtenerAnalisis(id);
        txtNombre.setText(dto.getNombreAnalisis());
        txtVnormal.setText(String.valueOf(dto.getValorNormal()));
        txtVcritico.setText(String.valueOf(dto.getValorCritico()));
        txtVReservado.setText(String.valueOf(dto.getValorReservado()));
        txtIdAnalisi.setText(String.valueOf(id));
    }


    private void okPressed() {
        DtoAnalisis dtoAnalisis = new DtoAnalisis();
        dtoAnalisis.setIdAnalisis(Integer.parseInt(txtIdAnalisi.getText()));
        dtoAnalisis.setValorCritico(Integer.parseInt(txtVcritico.getText()));
        dtoAnalisis.setValorReservado(Integer.parseInt(txtVReservado.getText()));
        dtoAnalisis.setValorNormal(Integer.parseInt(txtVnormal.getText()));
        dtoAnalisis.setNombreAnalisis(txtNombre.getText());

        controllerAnalisis.crearAnalisis(dtoAnalisis);
        if(frame != null){
            frame.dialogDisposed();
        }

        dispose();

    }
}
