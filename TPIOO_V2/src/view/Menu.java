package view;

import view.pacientes.CrearPacientesView;
import view.pacientes.PacientesView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Menu implements ActionListener {

    JMenu menu, menuPaciente, menuSucursal, menuPeticion, submenuPaciente, menuPracticas;

    JMenuItem menuPacienteAlta, menuPacienteModificacion, menuPacienteConsulta;
    JMenuItem menuSucursalConsulta, menuSucursalModificacion, menuSucursalAlta;
    JMenuItem menuPeticionConsulta, menuPeticionModificacion, menuPeticionAlta;
    JMenuItem menuPracticasConsulta, menuPracticasAlta, menuPracticasModificacion;

    JPanel panel1;
    JPanel panelView;
    JFrame frame = new JFrame("Ejemplo de Ventas");


    public Menu() {
        JMenuBar menuBar = new JMenuBar();
        //Menu principales
        menuPaciente = new JMenu("Paciente");
        menuPracticas = new JMenu("Practicas");
        menuSucursal = new JMenu("Sucursal");
        menuPeticion = new JMenu("Peticion");

        //Items-menu Paciente
        menuPacienteConsulta = new JMenuItem("Consulta");
        menuPacienteAlta = new JMenuItem("Alta");
        menuPacienteModificacion = new JMenuItem("Modificacion");
        menuPaciente.add(menuPacienteAlta);
        menuPaciente.add(menuPacienteConsulta);
        menuPaciente.add(menuPacienteModificacion);
        menuPacienteConsulta.addActionListener(this);
        menuPacienteAlta.addActionListener(this);
        menuPacienteModificacion.addActionListener(this);

        //Items-menu Sucursal
        menuSucursalConsulta = new JMenuItem("Consulta");
        menuSucursalAlta = new JMenuItem("Alta");
        menuSucursalModificacion = new JMenuItem("Modificacion");
        menuSucursal.add(menuSucursalAlta);
        menuSucursal.add(menuSucursalConsulta);
        menuSucursal.add(menuSucursalModificacion);
        menuSucursalConsulta.addActionListener(this);
        menuSucursalAlta.addActionListener(this);
        menuSucursalModificacion.addActionListener(this);

        //Items-menu Peticion
        menuPeticionConsulta = new JMenuItem("Consulta");
        menuPeticionAlta = new JMenuItem("Alta");
        menuPeticionModificacion = new JMenuItem("Modificacion");
        menuPeticion.add(menuPeticionAlta);
        menuPeticion.add(menuPeticionConsulta);
        menuPeticion.add(menuPeticionModificacion);
        menuPeticionConsulta.addActionListener(this);
        menuPeticionAlta.addActionListener(this);
        menuPeticionModificacion.addActionListener(this);

        //Items-menu Practicas
        menuPracticasConsulta = new JMenuItem("Consulta");
        menuPracticasAlta = new JMenuItem("Alta");
        menuPracticasModificacion = new JMenuItem("Modificacion");
        menuPracticas.add(menuPracticasAlta);
        menuPracticas.add(menuPracticasConsulta);
        menuPracticas.add(menuPracticasModificacion);
        menuPracticasConsulta.addActionListener(this);
        menuPracticasAlta.addActionListener(this);
        menuPracticasModificacion.addActionListener(this);

        menuBar.add(menuPaciente);
        menuBar.add(menuPeticion);
        menuBar.add(menuSucursal);
        menuBar.add(menuPracticas);
        frame.setJMenuBar(menuBar);

        frame.setSize(600, 600);
        frame.setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        panel1 = new JPanel();
        frame.add(panel1);

        frame.setVisible(true);
    }


    private void setPanelView(JPanel panel) {
        frame.setContentPane(panel);
        frame.validate();
        frame.repaint();
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource().equals(menuPacienteAlta)){
            CrearPacientesView crearPacientesView = new CrearPacientesView(404);
            setPanelView(crearPacientesView.pnlCrearPacientesView);
        }else if(e.getSource().equals(menuPacienteConsulta)){
            PacientesView pacientesView = new PacientesView();
            setPanelView(pacientesView.pnlPacientes);
        }
    }
}
