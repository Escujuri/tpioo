package model;

import contract.PermisosContract;
import dto.DtoPermisos;
import utils.JsonDb;

import java.util.List;

public class Permisos implements PermisosContract {
    private String permiso;
    private Boolean lectura;
    private Boolean escritura;

    private static List<Permisos> permisos;

    static {
        permisos = utils.JsonDb.getAll(Permisos.class);
    }

    public Permisos() {
    }

    public Permisos(String permiso, Boolean lectura, Boolean escritura) {
        this.permiso = permiso;
        this.lectura = lectura;
        this.escritura = escritura;
    }

    // -- GETTER AND SETTER--//
    public String getPermiso() {
        return permiso;
    }

    public void setPermiso(String permiso) {
        this.permiso = permiso;
    }

    public Boolean getLectura() {
        return lectura;
    }

    public void setLectura(Boolean lectura) {
        this.lectura = lectura;
    }

    public Boolean getEscritura() {
        return escritura;
    }

    public void setEscritura(Boolean escritura) {
        this.escritura = escritura;
    }

    @Override
    public Boolean crearPermiso(DtoPermisos permisoAux) {
        if (this.validarPermiso(permisoAux.getPermiso())) {
            return false;
        } else {
            this.permisos.add(new Permisos(permisoAux.getPermiso(), permisoAux.getLectura(), permisoAux.getEscritura()));
            JsonDb.save(Permisos.class, permisos);
            return true;
        }
    }

    @Override
    public Boolean borrarPermiso(String Permisos) {
        if (!this.validarPermiso(Permisos)) {
            return false;
        } else {
            this.permisos.remove(Permisos);
            JsonDb.save(Permisos.class, permisos);
            return true;
        }
    }

    @Override
    public Boolean modificarPermiso(DtoPermisos permisoAux) {
        if (this.validarPermiso(permisoAux.getPermiso())) {
            for (int i = 0; i < permisos.size(); i++) {
                if (permisos.get(i).getPermiso() == permiso) {
                    this.permisos.get(i).permiso = permiso;
                    this.permisos.get(i).escritura = escritura;
                    this.permisos.get(i).lectura = lectura;
                    JsonDb.save(Permisos.class, permisos);
                    return true;
                }
            }
        }
        return false;
    }

    private Boolean validarPermiso(String permiso) {
        for (int i = 0; i < permisos.size(); i++) {
            if (permisos.get(i).getPermiso() == permiso) {
                return true;
            }
        }
        return false;
    }
}