package contract;

import dto.DtoEstadosPeticiones;
import dto.DtoPeticion;

import java.util.List;

public interface PeticionesContract {

    Boolean crearPeticion(DtoPeticion peticion);

    Boolean eliminarPeticion(int idPeticion);

    Boolean actualizarPeticion(DtoPeticion peticion);

    Boolean actualizarEstadoPeticion(DtoEstadosPeticiones estadosPeticiones);

    List<DtoPeticion> obtenerPeticionesUsuarios(int dni);

    List<DtoPeticion> obtenerPeticionesSucursales (String nombreSucursal);

    List<DtoPeticion> obtenerPeticiones();

    DtoPeticion obtenerPeticion(int idPeticion);

    Boolean existePeticion(int idPeticion);
}
