package utils;

import com.google.common.io.*;
import com.google.gson.*;
import com.google.gson.reflect.*;

import java.io.*;
import java.lang.reflect.Type;
import java.util.List;

public class JsonDb {
    private static final Gson _gson = new GsonBuilder().setPrettyPrinting().create();

    public static <T> List<T> getAll(Class<T> entityClass) {
        Type entityListType = TypeToken.getParameterized(List.class, entityClass).getType();
        String entityJsonFile = getEntityDbFilePath(entityClass);

        List<T> entities = null;

        try (Reader reader = new FileReader(entityJsonFile)) {
             entities = _gson.fromJson(reader, entityListType);
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return entities;
    }

    public static <T> void save(Class<T> entityClass, List<T> entities) {
        String entityJsonFile = getEntityDbFilePath(entityClass);

        try (Writer writer = new FileWriter(entityJsonFile)) {
            _gson.toJson(entities, writer);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static <T> String getEntityDbFilePath(Class<T> entityClass) {
        String entityName = entityClass.getSimpleName();
        String entityJsonFile = Resources.getResource(String.format("resources/Data/%s.json", entityName)).getPath();
        return entityJsonFile;
    }
}