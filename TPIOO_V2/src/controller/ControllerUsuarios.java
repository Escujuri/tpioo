package controller;

import contract.UsuarioContract;
import dto.DtoUsuarios;
import model.Usuarios;

import java.util.List;

public class ControllerUsuarios {

    private static ControllerUsuarios cu = null;

    private UsuarioContract usuarioContract = new Usuarios();

    private ControllerUsuarios() {
    }

    public static synchronized ControllerUsuarios getInstance() {
        if (cu == null) {
            cu = new ControllerUsuarios();
        }
        return cu;
    }

    public Boolean crearUsuario(DtoUsuarios nuevoUsuario) {
        return usuarioContract.crearUsuario(nuevoUsuario);
    }

    public Boolean modificarUsuario(DtoUsuarios modificarUsuario) {
        return usuarioContract.modificarUsuario(modificarUsuario);
    }

    public Boolean borrarUsuario(int dni) {
        return usuarioContract.borrarUsuario(dni);
    }

    public Boolean asignarRol(int dni, String nombreRol) {
        return usuarioContract.asignarRol(dni, nombreRol);
    }

    public List<DtoUsuarios> obtenerUsuarios() {
        return usuarioContract.allUsuarios();
    }

    public DtoUsuarios obtenerUsuario(int id) {
        return usuarioContract.obtenerUsuario(id);
    }

    public Boolean existeUsuario(int dni) { return usuarioContract.existeUsuario(dni);}

}
