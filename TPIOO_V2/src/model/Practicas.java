package model;

import contract.PracticasContract;
import dto.DtoAnalisis;
import dto.DtoPracticas;
import utils.JsonDb;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Practicas implements PracticasContract {

    private String codPractica;

    private String nombrePractica;

    private int grupo;

    private int cantHorasResultado;

    private Boolean habilitado;

    public List<Analisis> getAnalisisList() {
        return analisisList;
    }

    public void setAnalisisList(List<Analisis> analisisList) {
        this.analisisList = analisisList;
    }

    private static List<Practicas> listaPracticas;

    private List<Analisis> analisisList = new ArrayList<>();

    public Practicas() {
    }

    static {
        listaPracticas = utils.JsonDb.getAll(Practicas.class);
    }

    public Practicas(String codPractica, String nombrePractica, int grupo, int cantHorasResultado, Boolean habilitado, List<Analisis> analisis) {
        this.codPractica = codPractica;
        this.nombrePractica = nombrePractica;
        this.grupo = grupo;
        this.cantHorasResultado = cantHorasResultado;
        this.habilitado = habilitado;
        this.analisisList = analisis;
    }

    @Override
    public Boolean crearPractica(DtoPracticas practica) {
        if (this.existePractica(practica.getCodPractica())) {
            return false;
        } else {

            Analisis analisis;
            List<Analisis> _analisisList = new ArrayList<>();

            if (practica.getAnalisisList() != null) {
                System.out.println("practica.getAnalisisList() no null");
                for (int i = 0; i < practica.getAnalisisList().size(); i++) {
                    analisis = new Analisis();
                    analisis.setNombreAnalisis(practica.getAnalisisList().get(i).getNombreAnalisis());
                    analisis.setIdAnalisis(practica.getAnalisisList().get(i).getIdAnalisis());
                    analisis.setValorCritico(practica.getAnalisisList().get(i).getValorCritico());
                    analisis.setValorNormal(practica.getAnalisisList().get(i).getValorNormal());
                    analisis.setValorReservado(practica.getAnalisisList().get(i).getValorReservado());
                    analisis.setResultado(practica.getAnalisisList().get(i).getResultado());
                    _analisisList.add(analisis);
                    System.out.println(practica.getAnalisisList().get(i).getNombreAnalisis());
                }
            }

            listaPracticas.add(new Practicas(practica.getCodPractica(), practica.getNombrePractica(),
                    practica.getGrupo(), practica.getCantHorasResultado(), practica.getHabilitado(),
                    _analisisList
            ));

            JsonDb.save(Practicas.class, listaPracticas);
            return true;
        }
    }

    @Override
    public Boolean actualizarPractica(DtoPracticas practica) {
        if (this.existePractica(codPractica)) {
            for (int i = 0; i < listaPracticas.size(); i++) {
                if (listaPracticas.get(i).getCodPractica() == codPractica) {
                    listaPracticas.get(i).codPractica = codPractica;
                    listaPracticas.get(i).nombrePractica = nombrePractica;
                    listaPracticas.get(i).grupo = grupo;
                    listaPracticas.get(i).cantHorasResultado = cantHorasResultado;
                    listaPracticas.get(i).habilitado = habilitado;
                    listaPracticas.get(i).analisisList = analisisList;
                    JsonDb.save(Practicas.class, listaPracticas);
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public List<DtoAnalisis> getTodosAnalisis() {
        DtoAnalisis result = null;
        List<DtoAnalisis> dtoAnalisisList = new ArrayList<>();
        for (Analisis analisis : analisisList) {
            result = new DtoAnalisis();
            result.setNombreAnalisis(analisis.getNombreAnalisis());
            result.setIdAnalisis(analisis.getIdAnalisis());
            result.setValorNormal(analisis.getValorNormal());
            result.setValorReservado(analisis.getValorReservado());
            result.setValorCritico(analisis.getValorCritico());
            result.setResultado(analisis.getResultado());
            dtoAnalisisList.add(result);
        }
        return dtoAnalisisList;
    }

    @Override
    public Boolean inhabilitarPractica(String codPractica) {
        if (!this.existePractica(codPractica)) {
            for (Practicas practica : listaPracticas) {
                if (Objects.equals(practica.getCodPractica(), codPractica)) {
                    practica.setHabilitado(false);
                    JsonDb.save(Practicas.class, listaPracticas);
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public DtoPracticas getPracticaById(String codPractica) {
        DtoPracticas object = null;
        for (Practicas practica : listaPracticas) {
            if (Objects.equals(practica.getCodPractica(), codPractica)) {
                object = new DtoPracticas();
                object.setHabilitado(practica.getHabilitado());
                object.setNombrePractica(practica.getNombrePractica());
                object.setGrupo(practica.getGrupo());
                object.setCantHorasResultado(practica.getCantHorasResultado());
                object.setAnalisisList(practica.getTodosAnalisis());
            }
        }
        return object;
    }

    @Override
    public Boolean existeValoresCriticos(String codPractica) {
        for (Practicas practica : listaPracticas) {
            if (Objects.equals(practica.getCodPractica(), codPractica)) {
                for (int i = 0; i < practica.getAnalisisList().size(); i++) {
                    if(practica.getAnalisisList().get(i).getValorCritico() < -1){
                    return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public Boolean existeValoresReservados(String codPractica) {
        for (Practicas practica : listaPracticas) {
            if (Objects.equals(practica.getCodPractica(), codPractica)) {
                for (int i = 0; i < practica.getAnalisisList().size(); i++) {
                    if(practica.getAnalisisList().get(i).getValorReservado() < -1){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public Analisis getAnalisisById(String idAnalisis) {
        Analisis retorno = null;
        for (Analisis an : analisisList) {
            if (an.isAnalisis(idAnalisis)) {
                retorno = an;
                break;
            }
        }
        return retorno;
    }


    private Boolean existePractica(String codPractica) {
        for (int i = 0; i < listaPracticas.size(); i++) {
            if (Objects.equals(listaPracticas.get(i).getCodPractica(), codPractica)) {
                return true;
            }
        }
        return false;
    }


    public List<DtoPracticas> getTodasPracticas() {
        DtoPracticas object = null;
        List<DtoPracticas> listaPracticasResponse = new ArrayList<DtoPracticas>();
        for (Practicas listaPractica : listaPracticas) {
            object = new DtoPracticas();
            object.setCodPractica(listaPractica.getCodPractica());
            object.setNombrePractica(listaPractica.getNombrePractica());
            object.setGrupo(listaPractica.getGrupo());
            object.setCantHorasResultado(listaPractica.getCantHorasResultado());
            object.setHabilitado(listaPractica.getHabilitado());
            object.setAnalisisList(listaPractica.getTodosAnalisis());
            listaPracticasResponse.add(object);
        }
        return listaPracticasResponse;
    }

    // -- GETTER AND SETTER--//

    public String getCodPractica() {
        return codPractica;
    }

    public void setCodPractica(String codPractica) {
        this.codPractica = codPractica;
    }

    public String getNombrePractica() {
        return nombrePractica;
    }

    public void setNombrePractica(String nombrePractica) {
        this.nombrePractica = nombrePractica;
    }

    public int getGrupo() {
        return grupo;
    }

    public void setGrupo(int grupo) {
        this.grupo = grupo;
    }

    public int getCantHorasResultado() {
        return cantHorasResultado;
    }

    public void setCantHorasResultado(int cantHorasResultado) {
        this.cantHorasResultado = cantHorasResultado;
    }

    public Boolean getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(Boolean habilitado) {
        this.habilitado = habilitado;
    }

}