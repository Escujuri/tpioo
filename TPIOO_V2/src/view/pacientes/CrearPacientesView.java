package view.pacientes;

import com.toedter.calendar.JDateChooser;
import controller.ControllerPacientes;
import dto.DtoPaciente;
import model.ObraSociales;
import model.Pacientes;
import model.Sexo;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Date;

public class CrearPacientesView extends JDialog {

    public JPanel pnlCrearPacientesView;
    private JTextField txtDNI;
    private JTextField txtNombreApellido;
    private JTextField txtMail;
    private JLabel lblDNI;
    private JLabel lblNombreApellido;
    private JLabel lblMail;
    private JLabel lblSexo;
    private JComboBox cmbSexo;
    private JLabel lblFechaNacimiento;
    private JCheckBox chkEnable;
    private JLabel lblObraSocial;
    private JComboBox cmbObraSocial;
    private JButton btnPacientesViewGuardar;
    private JButton btnPacientesViewCancel;
    private JPanel JDatePicker;
    private JButton btnDeletePaciente;
    private JFrame rootView = new JFrame("Pacientes");
    private DefaultComboBoxModel<ObraSociales> cmbObraSocialModel = new DefaultComboBoxModel();
    JDateChooser dateChooser = new JDateChooser();
    ControllerPacientes controllerPacientes = ControllerPacientes.getInstance();

    public CrearPacientesView(long pacienteId) {

        setTitle("Pacientes");
        setModal(true);
        setContentPane(pnlCrearPacientesView);
        setSize(400, 450);
        setLocationRelativeTo(null);
        getRootPane().setDefaultButton(btnPacientesViewGuardar);

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        JDatePicker.add(dateChooser);

        try {
            initializeControls(pacienteId);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage());
            return;
        }
    }

    public JPanel getPanel() {
        return new CrearPacientesView(404).pnlCrearPacientesView;
    }

    public void mostrar() {
        rootView.setVisible(true);
    }

    private void initializeControls(long pacienteId) {

        ObraSociales[] cmbObraSocialesOptions = controllerPacientes.getObraSociales().toArray(new ObraSociales[1]);
        cmbObraSocialModel = new DefaultComboBoxModel(cmbObraSocialesOptions);
        cmbObraSocial.setModel(cmbObraSocialModel);

        Sexo[] cmbSexoOptions = controllerPacientes.getSexoString();
        cmbSexo.setModel(new DefaultComboBoxModel(cmbSexoOptions));

        if (pacienteId == 404) {
            btnDeletePaciente.setEnabled(false);
        } else {

            btnDeletePaciente.setEnabled(true);

            DtoPaciente paciente = controllerPacientes.obtenerPaciente(pacienteId);
            txtDNI.setText(String.valueOf(paciente.getDni()));
            txtNombreApellido.setText(String.valueOf(paciente.getNombre()));
            txtMail.setText(String.valueOf(paciente.getEmail()));
            cmbSexo.setSelectedItem(paciente.getSexo());
            cmbObraSocial.setSelectedIndex(paciente.getCodObraSociales());
            chkEnable.setSelected(paciente.getHabilitado());
            cmbSexo.setSelectedItem(Sexo.valueOf(paciente.getSexo()));
            dateChooser.setDate(paciente.getFechaNacimiento());
        }

        btnPacientesViewGuardar.addActionListener(e -> onOK(pacienteId));
        btnPacientesViewCancel.addActionListener(e -> onCancel());
        btnDeletePaciente.addActionListener(e -> onBorrar(pacienteId));

    }

    private boolean validateDate(Date date) {
        boolean errorDate = true;

        if (date != null && controllerPacientes.calculateEdad(date) >= 0) {
            errorDate = false;
        }

        return errorDate;
    }

    private void onOK(long pacienteId) {
        if (txtDNI.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Debe ingresar un DNI para poder generar al paciente en sistema.");
            return;
        }
        if (txtNombreApellido.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Ingrese un Nombre y Apellido.");
            return;
        }
        if (txtMail.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Ingrese un email.");
            return;
        }
        if (validateDate(dateChooser.getDate())) {
            JOptionPane.showMessageDialog(this, "Ingrese una fecha valida");
            return;

        } else {

            DtoPaciente paciente = new DtoPaciente();
            paciente.setPacienteId(pacienteId);
            paciente.setDni(Integer.parseInt(txtDNI.getText()));
            paciente.setSexo(cmbSexo.getSelectedItem().toString());
            paciente.setNombre(txtNombreApellido.getText());
            paciente.setEmail(txtMail.getText());
            paciente.setHabilitado(chkEnable.isSelected());
            paciente.setCodObraSociales(controllerPacientes.getCodObraSociales(cmbObraSocial.getSelectedItem().toString()));
            System.out.println(cmbObraSocial.getSelectedItem().toString());
            System.out.println(controllerPacientes.getCodObraSociales(cmbObraSocial.getSelectedItem().toString()));
            paciente.setFechaNacimiento(dateChooser.getDate());

            try {
                controllerPacientes.save(paciente);
                JOptionPane.showMessageDialog(null, "Los datos se guardaron correctamente.");
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage());
                return;
            }
            dispose();
        }

    }

    private void onCancel() {
        dispose();
    }

    private void onBorrar(long pacienteId) {
        try {
            int codMessage = controllerPacientes.deletePaciente(pacienteId);
            if (codMessage == 500) {
                JOptionPane.showMessageDialog(null, "El paciente tiene peticiones cargadas.");
            }
            else if (codMessage == 404) {
                JOptionPane.showMessageDialog(null, "El paciente no fue encontrado.");
            }
            else if (codMessage == 200) {
                JOptionPane.showMessageDialog(null, "El paciente fue eliminado correctamente");
            }
            else {
                System.out.println("Error no contemplado en Delete paciente");
            }

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage());
            return;
        }
        dispose();
    }


}
