package model;

import contract.SucursalContract;
import controller.ControllerDirecciones;
import dto.DtoDireccion;
import dto.DtoPeticion;
import dto.DtoSucursales;
import utils.JsonDb;

import java.util.ArrayList;
import java.util.List;

public class Sucursales implements SucursalContract {

    private int numeroSucursal = 0;

    private Boolean habilitado;

    private String nombreSucursal;

    private String responsableTecnico;

    private Peticiones peticiones = new Peticiones();

    private DtoDireccion direccion;

    private static List<Sucursales> listaSucursales;

    private Direcciones direccionesService = new Direcciones();

    static {
        listaSucursales = utils.JsonDb.getAll(Sucursales.class);
    }

    @Override
    public List<DtoSucursales> allSucursales() {
        DtoSucursales object = null;
        List<DtoSucursales> listaSucursalesResponse = new ArrayList<DtoSucursales>();

        for (int i = 0; i < listaSucursales.size(); i++) {
            object = new DtoSucursales();
            object.setNumeroSucursal(listaSucursales.get(i).getNumeroSucursal());
            object.setNombreSucursal(listaSucursales.get(i).getNombreSucursal());
            object.setResponsableTecnico(listaSucursales.get(i).getResponsableTecnico());
            object.setHabilitado(listaSucursales.get(i).getHabilitado());
            DtoDireccion direccion = direccionesService.obtenerDireccionSucursal(listaSucursales.get(i).numeroSucursal);
            object.setDireccion(direccion);
            listaSucursalesResponse.add(object);
        }
        return listaSucursalesResponse;
    }

    @Override
    public DtoSucursales obtenerSucursal(int id) {
        DtoSucursales object;
        for (int i = 0; i < listaSucursales.size(); i++) {
            if (listaSucursales.get(i).getNumeroSucursal() == id) {
                object = new DtoSucursales();
                object.setNombreSucursal(listaSucursales.get(i).getNombreSucursal());
                object.setNumeroSucursal(listaSucursales.get(i).getNumeroSucursal());
                object.setHabilitado(listaSucursales.get(i).getHabilitado());
                return object;
            }
        }
        return null;
    }

    @Override
    public int obtenerIdSucursalByNombre(String nombreSucursal) {
        int idSucursal;
        for (int i = 0; i < listaSucursales.size(); i++) {
            if (listaSucursales.get(i).getNombreSucursal() == nombreSucursal) {
                idSucursal = listaSucursales.get(i).getNumeroSucursal();
                return idSucursal;
            }
        }
        return 0;
    }

    @Override
    public Boolean crearSucursal(DtoSucursales sucursal) {
        if (this.existeSucursal(sucursal.getNumeroSucursal())) {
            return false;
        } else {
            listaSucursales.add(new Sucursales(sucursal.getHabilitado(), sucursal.getNombreSucursal(), sucursal.getNumeroSucursal(), sucursal.getResponsableTecnico()));
            direccionesService.crearDireccionSucursal(sucursal.getDireccion());
            JsonDb.save(Sucursales.class, listaSucursales);
            return true;
        }
    }

    @Override
    public Boolean borrarSucursal(int numeroSucursal) {
        for (int i = 0; i < listaSucursales.size(); i++) {

            if (listaSucursales.get(i).getNumeroSucursal() == numeroSucursal) {

                List<DtoPeticion> peticionesBySucursales = peticiones.obtenerPeticionesSucursales(listaSucursales.get(i).getNombreSucursal());

                if(!peticionesBySucursales.contains(Estados.Finalizado)){

                DtoPeticion modSucursal;
                int sucursales = listaSucursales.size();

                if(sucursales==1) {
                    return false;
                }else {
                    for (int j = 0; j < peticionesBySucursales.size(); j++) {
                        modSucursal = new DtoPeticion();
                        modSucursal.setIdPeticion(peticionesBySucursales.get(j).getIdPeticion());
                        modSucursal.setNombreSucursal(listaSucursales.get(sucursales-1).getNombreSucursal());

                        peticiones.actualizarPeticion(
                                modSucursal
                        );
                    }
                }

                direccionesService.borrarDireccionSucursal(numeroSucursal);
                listaSucursales.remove(i);
                JsonDb.save(Sucursales.class, listaSucursales);
                return true;
                }
                return false;
            }
        }
        return false;
    }

    @Override
    public Boolean actualizarSucursal(DtoSucursales sucursal) {
        for (int i = 0; i < listaSucursales.size(); i++) {
            if (listaSucursales.get(i).getNumeroSucursal() == sucursal.getNumeroSucursal()) {
                if (sucursal.getNombreSucursal() != null)
                    listaSucursales.get(i).setNombreSucursal(sucursal.getNombreSucursal());
                if (sucursal.getHabilitado() != null)
                    listaSucursales.get(i).setHabilitado(sucursal.getHabilitado());
                if (sucursal.getResponsableTecnico() != null)
                    listaSucursales.get(i).setResponsableTecnico(sucursal.getResponsableTecnico());
                if (sucursal.getDireccion() != null)
                    direccionesService.modificarDireccionSucursal(sucursal.getDireccion());

                JsonDb.save(Sucursales.class, listaSucursales);
                return true;
            }
        }
        return false;
    }

    public Boolean existeSucursal(int numeroSucursal) {
        for (Sucursales listaSucursale : listaSucursales) {
            if (listaSucursale.getNumeroSucursal() == numeroSucursal) {
                return true;
            }
        }
        return false;
    }

    public Sucursales() {
    }

    public Sucursales(Boolean habilitado, String nombreSucursal, int numeroSucursal, String responsableTecnico) {
        this.numeroSucursal = numeroSucursal;
        this.habilitado = habilitado;
        this.nombreSucursal = nombreSucursal;
        this.responsableTecnico = responsableTecnico;
    }

    // -- GETTER AND SETTER--//
    public int getNumeroSucursal() {
        return numeroSucursal;
    }

    public void setNumeroSucursal(int numeroSucursal) {
        this.numeroSucursal = numeroSucursal;
    }

    public Boolean getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(Boolean habilitado) {
        this.habilitado = habilitado;
    }

    public String getNombreSucursal() {
        return nombreSucursal;
    }

    public void setNombreSucursal(String nombreSucursal) {
        this.nombreSucursal = nombreSucursal;
    }

    public String getResponsableTecnico() {
        return responsableTecnico;
    }

    public void setResponsableTecnico(String responsableTecnico) {
        this.responsableTecnico = responsableTecnico;
    }
}
