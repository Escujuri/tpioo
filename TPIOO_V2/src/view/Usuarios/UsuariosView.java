package view.Usuarios;
import com.sun.jdi.Value;
import controller.ControllerSucursales;
import controller.ControllerUsuarios;
import dto.DtoSucursales;
import dto.DtoUsuarios;
import view.sucursales.CrearSucursalView;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
public class UsuariosView {
    private JButton crearUsuarioButton;
    private JTable tblUsuarios;
    public JPanel pnlUsuarios;
    private JPanel pnlListaUsuarios;
    private DefaultTableModel tblUsuariosModel;
    CrearUsuario dialogForm;
    public  UsuariosView() {
        initView();
        setupButtons();
        loadUsuarios();
    }

    private void initView(){

        tblUsuariosModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tblUsuariosModel.addColumn("Dni");
        tblUsuariosModel.addColumn("Nombre");
        tblUsuariosModel.addColumn("Email");
        tblUsuarios = new JTable(tblUsuariosModel);
        tblUsuarios.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                if (me.getClickCount() == 2) {
                    int row = tblUsuarios.getSelectedRow();
                    int idUsuario = (int)tblUsuarios.getModel().getValueAt(row, 0);
                    showEditUsuario(idUsuario);
                }
            }
        });
        pnlListaUsuarios.setLayout(new BorderLayout());
        pnlListaUsuarios.add(new JScrollPane(tblUsuarios), BorderLayout.CENTER);
    }

    private void setupButtons(){
        crearUsuarioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showCrearUsuarios();
            }
        });
    }

    void showCrearUsuarios(){
        dialogForm = new CrearUsuario(this);
        dialogForm.setVisible(true);
    }

    void showEditUsuario(int id){
        dialogForm = new CrearUsuario(this);
        dialogForm.setEditing(id);
        dialogForm.setVisible(true);
    }

    public void dialogDisposd(){
        loadUsuarios();
    }

    private void loadUsuarios() {

        ControllerUsuarios controllerUsuarios =  ControllerUsuarios.getInstance();
        List<DtoUsuarios> usuarios =  controllerUsuarios.obtenerUsuarios();
        tblUsuariosModel.setRowCount(0);
        for (DtoUsuarios dto: usuarios) {

            tblUsuariosModel.addRow(new Object[] {
                    dto.getDni(),
                    dto.getNombreUsuario(),
                    dto.getEmail()
            });
        }
    }

}