package view;

import controller.ControllerSeguridad;
import view.Usuarios.UsuariosView;
import view.analisis.AnalisisView;
import view.pacientes.CrearPacientesView;
import view.pacientes.PacientesView;
import view.peticiones.PeticionesView;
import view.practicas.PracticasView;
import view.sucursales.SucursalesView;

import javax.swing.*;

public class MainForm {
    private JPanel pnlMain;
    private JTabbedPane tabbedPane1;
    private JPanel pnlPacientes;
    private JPanel pnlPracticas;
    private JPanel pnlSucursales;
    private JPanel pnlPeticiones;
    private JPanel pnlUsuarios;
    private JPanel pnlAnalisis;
    ControllerSeguridad controllerSeguridad = ControllerSeguridad.getInstance();
    JFrame frame = new JFrame("Sistema de Gestión de Practicas");

    public MainForm() {
        //Menu principales
        setTabsAndViews();
        frame.setSize(700, 600);
        frame.setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setContentPane(tabbedPane1);
        frame.setVisible(true);
        System.out.println("El rol seteado es " + controllerSeguridad.obtenerRolActual() );
    }

    /*
    * Para agregar un nuevo tab se debe:
    * Agregar un panel en archivo MainForm.form
    * agregar en setTabsAndViews el panel de la clase view correspondiente a ese tab
    * */

   private void setTabsAndViews(){
       tabbedPane1 = new JTabbedPane();

       PacientesView pacientesView = new PacientesView();
       PracticasView practicasView = new PracticasView();
       SucursalesView sucursalesView = new SucursalesView();
       PeticionesView peticionesView = new PeticionesView();
       UsuariosView usuariosView = new UsuariosView();
       AnalisisView analisisView = new AnalisisView();

        tabbedPane1.addTab("Pacientes", null, pacientesView.pnlPacientes,
                "Abre tab pacientes");

        tabbedPane1.addTab("Practicas", null, practicasView.pnlPracticas,
                "Abre tab Practicas");

       tabbedPane1.addTab("Sucursales", null, sucursalesView.pnlSucursales,
               "Abre tab Sucursales");

       tabbedPane1.addTab("Peticiones", null, peticionesView.pnlPeticiones,
               "Abre tab Peticiones");

       tabbedPane1.addTab("Usuarios", null, usuariosView.pnlUsuarios,
               "Abre tab Usuarios");

       tabbedPane1.addTab("Analisis", null, analisisView.pnlAnalisis,
               "Abre tab Analisi");

    }
}
