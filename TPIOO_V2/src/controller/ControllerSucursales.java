package controller;

import contract.SucursalContract;
import dto.DtoSucursales;
import model.Sucursales;

import java.util.List;

public class ControllerSucursales {

    private static ControllerSucursales cu = null;

    SucursalContract sucursalContract = new Sucursales();

    private ControllerSucursales() {
    }

    public static synchronized ControllerSucursales getInstance() {
        if (cu == null) {
            cu = new ControllerSucursales();
        }
        return cu;
    }

    public List<DtoSucursales> obtenerSucursales() {
        return sucursalContract.allSucursales();
    }

    public DtoSucursales obtenerSucursal(int idSucursal) {
        return sucursalContract.obtenerSucursal(idSucursal);
    }

    public Boolean crearSucursal(DtoSucursales sucursalDTO) {
        return sucursalContract.crearSucursal(sucursalDTO);
    }

    public Boolean actualizarSucursal(DtoSucursales sucursal) {
        return sucursalContract.actualizarSucursal(sucursal);
    }

    public Boolean borrarSucursal(int idSucursal) {
        return sucursalContract.borrarSucursal(idSucursal);
    }

    public Boolean existeSucursal(int numeroSucursal) {
        return  sucursalContract.existeSucursal(numeroSucursal);
    };

    public int obtenerIdSucursalByNombre(String nombreSucursal)  {
        return  sucursalContract.obtenerIdSucursalByNombre(nombreSucursal);
    };

}
