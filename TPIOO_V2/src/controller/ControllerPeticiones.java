package controller;

import contract.PeticionesContract;
import dto.DtoEstadosPeticiones;
import dto.DtoPeticion;
import model.Peticiones;

import java.util.List;

public class ControllerPeticiones {

    private static ControllerPeticiones cu = null;

    private PeticionesContract peticionesContract = new Peticiones();

    private ControllerPeticiones() {
    }

    public static synchronized ControllerPeticiones getInstance() {
        if (cu == null) {
            cu = new ControllerPeticiones();
        }
        return cu;
    }

    public Boolean crearPeticion(DtoPeticion peticion) {
        return peticionesContract.crearPeticion(peticion);
    }

    public Boolean eliminarPeticion(int idPeticion) {
        return peticionesContract.eliminarPeticion(idPeticion);
    }

    public Boolean actualizarPeticion(DtoPeticion peticion) {
        return peticionesContract.actualizarPeticion(peticion);
    }

    public List<DtoPeticion> obtenerPeticionesUsuarios(int dni) {
        return peticionesContract.obtenerPeticionesUsuarios(dni);
    }

    public List<DtoPeticion> obtenerPeticionesSucursales(int dni) {
        return peticionesContract.obtenerPeticionesUsuarios(dni);
    }

    public List<DtoPeticion> obtenerPeticiones() {
        return peticionesContract.obtenerPeticiones();
    }

    public Boolean actualizarEstadoPeticion(DtoEstadosPeticiones estadosPeticiones) {
        return peticionesContract.actualizarEstadoPeticion(estadosPeticiones);
    }

    public DtoPeticion obtenerPeticion(int idPeticion) {
        return peticionesContract.obtenerPeticion(idPeticion);
    }

    public Boolean existePeticion(int idPeticion) {
        return peticionesContract.existePeticion(idPeticion);
    }

}
