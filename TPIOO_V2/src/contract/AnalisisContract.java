package contract;

import dto.DtoAnalisis;

import java.util.List;

public interface AnalisisContract {

    DtoAnalisis obtenerAnalisis(int idAnalisis);

    List<DtoAnalisis> getTodosAnalisis();

    Boolean crearAnalisis(DtoAnalisis Analisis);

    Boolean borrarAnalisis(int idAnalisis);

    Boolean editarAnalisis(DtoAnalisis Analisis);

}
