package model;

import contract.RolesContract;
import dto.DtoRoles;
import utils.JsonDb;

import java.util.ArrayList;
import java.util.List;

public class Roles implements RolesContract {

    private String nombreRol;

    private static List<Roles> roles;

    private Permisos permisosContract = new Permisos();

    public Roles() {
    }

    static {
        roles = utils.JsonDb.getAll(Roles.class);
    }

    public Roles(String nombreRol) {
        this.nombreRol = nombreRol;
    }

    public String getNombreRol() {
        return nombreRol;
    }

    public void setNombreRol(String nombreRol) {
        this.nombreRol = nombreRol;
    }

    @Override
    public Boolean crearRol(DtoRoles rol) {
        if (this.validarRol(rol.getNombreRol())) {
            return false;
        } else {
            roles.add(new Roles(rol.getNombreRol()));
            if (permisosContract.crearPermiso(rol.getPermisos())) {
                JsonDb.save(Roles.class, roles);
                return true;
            }
            return false;
        }
    }

    @Override
    public Boolean borrarRol(String NombreRol) {
        if (!this.validarRol(NombreRol)) {
            return false;
        } else {
            roles.remove(nombreRol);
            if (permisosContract.borrarPermiso(nombreRol)) {
                JsonDb.save(Roles.class, roles);
                return true;
            }
            return false;
        }
    }

    @Override
    public List<DtoRoles> obtenerRoles() {
        List<DtoRoles> dtoRoles = new ArrayList<>();
        DtoRoles object;
        for (int i = 0; i < roles.size(); i++) {
            object = new DtoRoles();
            object.setNombreRol(roles.get(i).nombreRol);
            dtoRoles.add(object);
        }
        return dtoRoles;
    }

    @Override
    public void setearRolActual(DtoRoles dtoRole) {
        this.nombreRol = dtoRole.getNombreRol();
    }

    @Override
    public String obtenerRolActual() {
        return this.nombreRol;
    }

    @Override
    public Boolean modificarRol(DtoRoles rol) {
        if (this.validarRol(nombreRol)) {
            for (int i = 0; i < roles.size(); i++) {
                if (roles.get(i).getNombreRol() == nombreRol) {
                    roles.get(i).nombreRol = nombreRol;
                    if (permisosContract.modificarPermiso(rol.getPermisos())) {
                        JsonDb.save(Roles.class, roles);
                        return true;
                    }
                    return false;
                }
            }
        }
        return false;
    }

    private Boolean validarRol(String nombreRol) {
        for (int i = 0; i < roles.size(); i++) {
            if (roles.get(i).getNombreRol() == nombreRol) {
                return true;
            }
        }
        return false;
    }


}

